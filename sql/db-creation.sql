CREATE SCHEMA IF NOT EXISTS exhibition;
USE exhibition;

DROP TRIGGER IF EXISTS set_ticket_price;

DROP TABLE IF EXISTS exposition_rooms;
DROP TABLE IF EXISTS exposition_schedule;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS ticket;
DROP TABLE IF EXISTS exposition;
DROP TABLE IF EXISTS user_account;

--
-- tables with main entites
--
CREATE TABLE exposition (
    expo_id INT AUTO_INCREMENT NOT NULL,
    ticket_price DECIMAL(6 , 2 ) NOT NULL,
    theme_ua VARCHAR(255) NOT NULL,
    description_ua TEXT,
    theme_en VARCHAR(255) NOT NULL,
    description_en TEXT,
    PRIMARY KEY (expo_id)
)  ENGINE=InnoDB DEFAULT CHARSET=UTF8;

CREATE TABLE room (
    room_id INT AUTO_INCREMENT NOT NULL,
    room_num TINYINT UNSIGNED NOT NULL,
    floor TINYINT UNSIGNED NOT NULL,
    PRIMARY KEY (room_id),
    CONSTRAINT unique_room UNIQUE (room_num , floor)
)  ENGINE=InnoDB DEFAULT CHARSET=UTF8;

CREATE TABLE user_account (
	user_id INT AUTO_INCREMENT NOT NULL,
    login VARCHAR(20) NOT NULL,
    passw VARCHAR(20) NOT NULL,
    user_role ENUM('user', 'admin'),
    name VARCHAR(20) NOT NULL,
    surname VARCHAR(20) NOT NULL,
    email VARCHAR(40) NOT NULL,
    PRIMARY KEY (user_id),
    UNIQUE (login),
    UNIQUE (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
CREATE TABLE ticket (
	ticket_id INT AUTO_INCREMENT NOT NULL,
    user_id INT NOT NULL,
    expo_id INT NOT NULL,
    payment_status ENUM('paid', 'unpaid') DEFAULT 'unpaid',
    creation_time DATETIME NOT NULL DEFAULT NOW(),
    visitors_amount INT NOT NULL DEFAULT 1,
    for_date DATE NOT NULL,
    price DECIMAL(6, 2) NOT NULL DEFAULT 0,
    PRIMARY KEY (ticket_id),
    FOREIGN KEY (user_id) REFERENCES user_account(user_id) ON DELETE CASCADE,
    FOREIGN KEY (expo_id) REFERENCES exposition(expo_id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- auxiliary tables 
--

CREATE TABLE exposition_rooms (
	id INT AUTO_INCREMENT NOT NULL,
    expo_id INT NOT NULL,
    room_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (room_id) REFERENCES room(room_id) ON DELETE CASCADE,
    FOREIGN KEY (expo_id) REFERENCES exposition(expo_id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE exposition_schedule (
	schedule_id INT AUTO_INCREMENT NOT NULL,
	expo_id INT NOT NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    start_day DATE NOT NULL,
    end_day DATE NOT NULL,
    PRIMARY KEY(schedule_id),
    FOREIGN KEY (expo_id) REFERENCES exposition(expo_id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

-- /////////////////////  
-- //// Stored procedure
-- /////////////////////
DELIMITER $$
CREATE PROCEDURE GetAvailableRooms(
	IN user_start_day DATE,
    IN user_end_date DATE
)
BEGIN            
	-- Select all rooms available at the entered time 
	SELECT * FROM room WHERE room_id NOT IN (
		-- Get all rooms, which are occupied at the time entered by user
		SELECT exposition_rooms.room_id
		FROM exposition_rooms
		WHERE exposition_rooms.expo_id IN (
        
                -- Get all exposition_IDs, which are at the at the time entered by user
				SELECT DISTINCT exposition_schedule.expo_id 
				FROM exposition_schedule
                
                -- comparsion: user start date and end date shouldn't be at the [start_date - end_date] period of time
				WHERE start_day BETWEEN user_start_day AND user_end_date
					OR end_day BETWEEN user_start_day AND user_end_date
					OR (start_day <= user_start_day AND end_day >= user_end_date)
			)
		) ORDER BY room.room_id ASC;
END$$
DELIMITER ;

-- /////////////////////
-- ///////////// Trigger
-- /////////////////////
DELIMITER $$
CREATE TRIGGER set_ticket_price BEFORE INSERT ON ticket FOR EACH ROW
BEGIN
	SET NEW.price = (SELECT exposition.ticket_price FROM exposition WHERE exposition.expo_id=NEW.expo_id) * NEW.visitors_amount;
END$$
DELIMITER ;