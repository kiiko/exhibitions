USE exhibition;

INSERT INTO room (room_num, floor) VALUES 
	(1, 1), (2, 1), (3, 1), (4, 1),
	(5, 1), (6, 1), (7, 1), (8, 1),
	(9, 1), (10, 1),
	(1, 2), (2, 2), (3, 2), (4, 2),
	(5, 2), (6, 2), (7, 2), (8, 2),
	(9, 2), (10, 2),
	(1, 3), (2, 3), (3, 3), (4, 3),
	(5, 3), (6, 3), (7, 3), (8, 3),
	(9, 3), (10, 3);
    
INSERT INTO user_account (email, login, passw, user_role, name, surname) VALUES 
	('vasylkiikofw@gmail.com', 'admin', 'admin', 'admin', 'Vasyl', 'Kiiko'),
	('sofia@gmail.com', 'sofia', 'Password1!', 'user', 'Sofia', 'Kiiko'),
    ('dimeduna@gmail.com', 'medu_na', 'password', 'user', 'Діана', 'Медуна'), 
    ('test@gmail.com', 'login', 'password', 'user', 'Ivan', 'Moroz');
    
INSERT INTO exposition(ticket_price, theme_ua, description_ua, theme_en, description_en) VALUES 
(50, 'Картини XV століття', 'Представлення картин таких видатних художників як Леонардо да Вінчі, Сандро Боттічеллі та інших.', 
'Paintings from XV century', 'Presentation of paintings by such outstanding artists as Leonardo da Vinci, Sandro Botticelli and others.'), 
(75.75, 'Виставка-аукціон "Сучасне українське мистецтво"', 'Представлено 40 творів (живопис, графіка, скульптура) українських митців ХХ – початку ХХІ ст', 
'Exhibition-auction "Contemporary Ukrainian Art"', 'There are 40 works (paintings, graphics, sculpture) of Ukrainian artists of the XX - early XXI centuries'), 
(60, 'Україна XIX століття', 'Представлення елементів побуту українців часів XIX століття', 
'Ukraine in XIX century', 'Representation of elements of life of Ukrainians of the XIX century'),
(100, 'Європейське надбання епохи відродження', 'Виставка творінь видатних митців епохи відродження', 
'European heritage of the Renaissance', 'Exhibition of works by outstanding artists of the Renaissance'),
(150, 'Сучасні митці: як творить Україна', 'Представлення відомих митців сучасності та їх творінь', 
'Contemporary artists: how Ukraine creates', 'Presentation of famous contemporary artists and their works'),
(65, 'Промисловий Львів - історія', 'Історія розвитку львівських промисловостей та їх теперішній стан', 
'Industrial Lviv - history', 'History of development of Lviv industries and their current state'),
(170, 'Виставка ювелірних прикрас', 'На виставці будуть представлені найбільш витончені прикраси з золота та срібла. Ви зможете побачити дорогоцінні камені та обрати прикрасу дорогій людині.', 
'Exhibition of jewelry', 'The exhibition will feature the most exquisite jewelry made of gold and silver. You will be able to see precious stones and choose jewelry for a loved one.'),
(95, 'Рослини-ендеміки', 'Виставка покаже рідкісні види рослин, які ростуть лише на територііі України. Ви прогуляєтесь своєрідним ботанічним садом та будете спостерігати за красою українських ендеміків.', 
'Endemic plants', 'The exhibition will show rare species of plants that grow only in Ukraine. You will walk through a kind of botanical garden and will observe the beauty of Ukrainian endemics.'),
(100, 'Українські винаходи, які змінять світ', 'У нас представлені 50 найкращих приладів, створених українськими винахідниками. Вигадливість наших майстрів здивує навіть найбільших скептиків.', 
'Ukrainian inventions that will change the world', 'We present the 50 best devices created by Ukrainian inventors. The ingenuity of our masters will surprise even the biggest skeptics.'),
(200, 'Футбольні здобутки львівських "Карпат"', 'Маєте унікальну можливість переглянути історію здобутків футбольного клубу "Карпати" та поспілкуватись з людьми, які здобували ці досягнення.', 
'Football achievements of Lviv "Karpaty"', 'You have a unique opportunity to view the history of the achievements of the football club "Karpaty" and chat with people who have achieved these achievements.'),
(75.50, 'Гуцули крізь роки', 'Ви зможете побачити та порівняти, як змінювався гуцульський декор та образи впродовж 200 років. Наші експонати - це найповніша картина карпатського життя з 1800-их років до сьогодення.', 
'Hutsuls over the years', 'You will be able to see and compare how Hutsul decor and images have changed over 200 years. Our exhibits are the most complete picture of Carpathian life from the 1800s to the present.');

INSERT INTO exposition_schedule(expo_id, start_time, end_time, start_day, end_day) VALUES 
	(1, '10:00:00', '18:00:00', '2021-03-15', '2021-03-19'),
    (1, '12:00:00', '20:00:00', '2021-03-20', '2021-03-21'),
    (2, '12:00:00', '19:00:00', '2021-03-08', '2021-03-21'),
    (3, '09:00:00', '15:00:00', '2021-03-18', '2021-03-19'),
    (3, '09:00:00', '19:00:00', '2021-03-20', '2021-03-21'),
    (4, '10:00:00', '16:00:00', '2021-03-29', '2021-04-03'),
    (5, '12:00:00', '19:00:00', '2021-03-20', '2021-03-21'),
    (6, '09:00:00', '14:00:00', '2021-03-01', '2021-03-05'), 
    (6, '09:00:00', '18:00:00', '2021-03-06', '2021-03-07'),
    (7, '10:00:00', '18:00:00', '2021-03-22', '2021-03-28'),
    (8, '09:00:00', '18:00:00', '2021-04-05', '2021-04-09'),
    (8, '10:00:00', '20:00:00', '2021-04-10', '2021-04-11'), 
    (9, '11:30:00', '16:00:00', '2021-03-17', '2021-03-19'),
    (10, '12:00:00', '16:00:00', '2021-03-29', '2021-04-02'),
    (10, '12:00:00', '19:00:00', '2021-04-03', '2021-04-04'),
    (11, '10:00:00', '14:00:00', '2021-03-15', '2021-03-18');
    

INSERT INTO exposition_rooms(expo_id, room_id) VALUES 
(1, 1), (1, 2), (1, 3), (1, 4),
(2, 11), (2, 12), (2, 13),
(3, 14),
(4, 15), 
(5, 5), (5, 6),
(6, 21), (6, 22), 
(7, 23), (7, 24), (7, 25),
(8, 7), (8, 8), (8, 9), (8, 10),
(9, 16), (9, 17),
(10, 26), (10, 27),
(11, 18), (11, 19);

INSERT INTO ticket (user_id, expo_id) VALUES (2, 1);
INSERT INTO ticket (user_id, expo_id) VALUES (2, 2), (3, 4), (4, 7);
