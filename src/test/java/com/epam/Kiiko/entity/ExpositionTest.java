package com.epam.Kiiko.entity;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ExpositionTest {
    @Test
    public void verifyReturnStartAndEndDates() {
        ExpositionSchedule schedule1 = new ExpositionSchedule
                .Builder()
                .withStartDate(LocalDate.parse("2021-02-10"))
                .withEndDate(LocalDate.parse("2021-02-12"))
                .withStartTime(LocalTime.parse("10:00:00"))
                .withEndTime(LocalTime.parse("20:00:00"))
                .build();
        ExpositionSchedule schedule2 = new ExpositionSchedule
                .Builder()
                .withStartDate(LocalDate.parse("2021-02-08"))
                .withEndDate(LocalDate.parse("2021-02-09"))
                .withStartTime(LocalTime.parse("11:00:00"))
                .withEndTime(LocalTime.parse("19:00:00"))
                .build();

        List<ExpositionSchedule> schedules = new ArrayList<>();
        schedules.add(schedule1);
        schedules.add(schedule2);

        Exposition exposition = new Exposition();
        exposition.setSchedule(schedules);
        assertEquals(LocalDate.parse("2021-02-08"), exposition.getStartDate());
        assertEquals(LocalDate.parse("2021-02-12"), exposition.getLastDate());
    }

    @Test
    public void verifyPrintRooms() {
        List<Room> rooms = new ArrayList<>();
        Room room1 = new Room();
        Room room2 = new Room();
        room1.setRoomId(11);
        room2.setRoomId(21);
        rooms.add(room1);
        rooms.add(room2);

        Exposition exposition = new Exposition();
        exposition.setRooms(rooms);

        String expected = "11, 21";
        assertEquals(expected, exposition.printRooms());
    }
}