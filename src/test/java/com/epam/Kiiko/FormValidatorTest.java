package com.epam.Kiiko;

import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class FormValidatorTest {
    @Test
    public void verifyReturnFalseIfParamIsNotEmpty() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("name")).thenReturn("abc");
        when(request.getParameter("surname")).thenReturn("xyz");
        String[] params = new String[]{"name", "surname"};
        assertFalse(FormValidator.isParamEmpty(request, params));
    }

    @Test
    public void verifyReturnTrueIfParamIsEmpty() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("name")).thenReturn("qwerty");
        when(request.getParameter("surname")).thenReturn(null);
        String[] params = new String[]{"name", "surname"};
        assertTrue(FormValidator.isParamEmpty(request, params));
    }

    @Test
    public void verifyIsIntegerWorksAppropriate() {
        assertTrue(FormValidator.isInteger("123"));
        assertFalse(FormValidator.isInteger("123z"));
        assertFalse(FormValidator.isInteger("12.3"));
    }

    @Test
    public void verifyIsDoubleWorksAppropriate() {
        assertTrue(FormValidator.isDouble("123"));
        assertFalse(FormValidator.isDouble("123z"));
        assertTrue(FormValidator.isDouble("12.3"));
    }

    @Test
    public void verifyIsEmailValidWorksAppropriate() {
        assertTrue(FormValidator.isEmailValid("vasyl@gmail.com"));
        assertTrue(FormValidator.isEmailValid("stu123dent@nulp.ua"));
        assertFalse(FormValidator.isEmailValid("vasyl.gmail.com"));
    }

    @Test
    public void verifyIsDateStringValidWorksAppropriate() {
        assertTrue(FormValidator.isDateStringValid("2021-12-12"));
        assertFalse(FormValidator.isDateStringValid("2021-13-13"));
        assertFalse(FormValidator.isDateStringValid("2021-13-12"));
        assertFalse(FormValidator.isDateStringValid("12-12-2021"));
        assertFalse(FormValidator.isDateStringValid("2021/12/10"));

    }

    @Test
    public void verifyIsTimeStringValidWorksAppropriate() {
        assertTrue(FormValidator.isTimeStringValid("10:15:30"));
        assertFalse(FormValidator.isTimeStringValid("10:100:30"));
        assertFalse(FormValidator.isTimeStringValid("33-15-30"));
        assertFalse(FormValidator.isTimeStringValid("100:15:30"));
        assertFalse(FormValidator.isTimeStringValid("10"));

    }

    @Test
    public void verifyReturnTrueIfDateIsValid() {
        assertTrue(FormValidator.isDateValid("2021-12-12", "2021-12-13"));
        assertTrue(FormValidator.isDateValid("2021-10-12", "2021-12-10"));
    }

    @Test
    public void verifyReturnFalseIfDateIsInvalid() {
        assertFalse(FormValidator.isDateValid("2021-12-13", "2021-12-13"));
        assertFalse(FormValidator.isDateValid("2021-12-10", "2021-10-12"));
        assertFalse(FormValidator.isDateValid("2021-10-12", "2021-10-10"));
        assertFalse(FormValidator.isDateValid("2021/12/13", "2021/12/13"));
    }

    @Test
    public void verifyReturnTrueIfTimeIsValid() {
        assertTrue(FormValidator.isTimeValid("10:15:30", "10:19:30"));
        assertTrue(FormValidator.isTimeValid("10:15:30", "22:15:30"));
    }

    @Test
    public void verifyReturnFalseIfTimeIsInvalid() {
        assertFalse(FormValidator.isTimeValid("10:15:30", "10:15:30"));
        assertFalse(FormValidator.isTimeValid("10:17:30", "10:15:30"));
        assertFalse(FormValidator.isTimeValid("17:15:30", "10:15:30"));
        assertFalse(FormValidator.isTimeValid("9-15-30", "10-15-30"));
    }
}