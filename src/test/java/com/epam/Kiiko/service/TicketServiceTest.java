package com.epam.Kiiko.service;

import com.epam.Kiiko.dao.TicketDAO;
import com.epam.Kiiko.exception.DatabaseException;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

public class TicketServiceTest {

    @Test
    public void verifyReturnFalseIfTicketIsNotPaidAndBelongToUser() throws DatabaseException {
        TicketDAO ticketDAO = mock(TicketDAO.class);
        when(ticketDAO.ticketBelongToUser(1, 1)).thenReturn(true);
        when(ticketDAO.isAlreadyPaid(1)).thenReturn(false);
        TicketService ticketService = new TicketService(ticketDAO);
        ticketService.isInvalidTicketStatusForUser(1, 1);
        verify(ticketDAO, times(1)).ticketBelongToUser(1, 1);
        verify(ticketDAO, times(1)).isAlreadyPaid(1);

        Assert.assertFalse(ticketService.isInvalidTicketStatusForUser(1, 1));
    }

    @Test
    public void verifyReturnTrueIfTicketNotBelongToUser() throws DatabaseException {
        TicketDAO ticketDAO = mock(TicketDAO.class);
        when(ticketDAO.ticketBelongToUser(2, 1)).thenReturn(false);
        when(ticketDAO.isAlreadyPaid(1)).thenReturn(false);
        TicketService ticketService = new TicketService(ticketDAO);
        ticketService.isInvalidTicketStatusForUser(2, 1);
        verify(ticketDAO, times(1)).ticketBelongToUser(2, 1);
        verify(ticketDAO, times(0)).isAlreadyPaid(2);

        Assert.assertTrue(ticketService.isInvalidTicketStatusForUser(2, 1));
    }

    @Test
    public void verifyReturnTrueIfTicketIsAlreadyPaid() throws DatabaseException {
        TicketDAO ticketDAO = mock(TicketDAO.class);
        when(ticketDAO.ticketBelongToUser(3, 1)).thenReturn(true);
        when(ticketDAO.isAlreadyPaid(3)).thenReturn(true);
        TicketService ticketService = new TicketService(ticketDAO);
        ticketService.isInvalidTicketStatusForUser(3, 1);
        verify(ticketDAO, times(1)).ticketBelongToUser(3, 1);
        verify(ticketDAO, times(1)).isAlreadyPaid(3);

        Assert.assertTrue(ticketService.isInvalidTicketStatusForUser(3, 1));

    }
}