package com.epam.Kiiko.service;

import com.epam.Kiiko.dao.ExpositionDAO;
import com.epam.Kiiko.dao.ExpositionScheduleDAO;
import com.epam.Kiiko.dao.RoomDAO;
import com.epam.Kiiko.entity.Exposition;
import com.epam.Kiiko.entity.ExpositionSchedule;
import com.epam.Kiiko.entity.Room;
import com.epam.Kiiko.entity.UserRole;
import com.epam.Kiiko.exception.DatabaseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class ExpositionServiceTest {
    private ExpositionDAO expositionDAO;
    private ExpositionScheduleDAO scheduleDAO;
    private RoomDAO roomDAO;

    @Before
    public void initMocks() {
        expositionDAO = mock(ExpositionDAO.class);
        scheduleDAO = mock(ExpositionScheduleDAO.class);
        roomDAO = mock(RoomDAO.class);
    }

    @Test
    public void verifyGetExpoListReturnExpoList() throws DatabaseException {
        Exposition exposition = new Exposition.Builder()
                .withExpoId(1)
                .withPrice(1)
                .withExpoThemeEn("eng")
                .withExpoThemeUa("ukr")
                .withDescriptionEn("description")
                .withDescriptionUa("опис")
                .build();
        List<Exposition> expositions = new ArrayList<>();
        expositions.add(exposition);

        ExpositionSchedule schedule = new ExpositionSchedule.Builder()
                .withStartDate(LocalDate.parse("2021-02-10"))
                .withEndDate(LocalDate.parse("2021-02-12"))
                .withStartTime(LocalTime.parse("10:00:00"))
                .withEndTime(LocalTime.parse("20:00:00"))
                .build();
        List<ExpositionSchedule> schedules = new ArrayList<>();
        schedules.add(schedule);

        Room room = new Room();
        room.setRoomId(1);
        List<Room> rooms = new ArrayList<>();
        rooms.add(room);

        ExpoSearchDetails searchDetails = new ExpoSearchDetails("en", "default", UserRole.USER);

        when(expositionDAO.getExpoList(searchDetails)).thenReturn(expositions);
        when(scheduleDAO.getScheduleForExposition(1)).thenReturn(schedules);
        when(roomDAO.getExpositionRooms(1)).thenReturn(rooms);

        ExpositionService expoService = new ExpositionService(expositionDAO, scheduleDAO, roomDAO);
        List<Exposition> testExpo = expoService.getExpositionList(searchDetails);
        Assert.assertEquals(schedules, testExpo.get(0).getSchedule());
        Assert.assertEquals(rooms, testExpo.get(0).getRooms());
        Assert.assertEquals("eng", testExpo.get(0).getExpoTheme());
        Assert.assertEquals("description", testExpo.get(0).getDescription());
    }

    @Test
    public void verifyReturnExpoCount() throws DatabaseException {
        ExpoSearchDetails searchDetails = new ExpoSearchDetails("en", "default", UserRole.USER);
        when(expositionDAO.getExpoCount(searchDetails)).thenReturn(1);
        ExpositionService expositionService = new ExpositionService(expositionDAO, scheduleDAO, roomDAO);
        Assert.assertEquals(1, expositionService.getExpositionCount(searchDetails));
    }

    @Test
    public void verifyGetExpoDetailsReturnExposition() throws DatabaseException {
        Exposition exposition = new Exposition.Builder()
                .withExpoId(1)
                .withPrice(1)
                .withExpoThemeEn("eng")
                .withExpoThemeUa("ukr")
                .withDescriptionEn("description")
                .withDescriptionUa("опис")
                .build();

        ExpositionSchedule schedule = new ExpositionSchedule.Builder()
                .withStartDate(LocalDate.parse("2021-02-10"))
                .withEndDate(LocalDate.parse("2021-02-12"))
                .withStartTime(LocalTime.parse("10:00:00"))
                .withEndTime(LocalTime.parse("20:00:00"))
                .build();
        List<ExpositionSchedule> schedules = new ArrayList<>();
        schedules.add(schedule);

        Room room = new Room();
        room.setRoomId(1);
        List<Room> rooms = new ArrayList<>();
        rooms.add(room);

        when(expositionDAO.getExpositionById(1)).thenReturn(exposition);
        when(scheduleDAO.getScheduleForExposition(1)).thenReturn(schedules);
        when(roomDAO.getExpositionRooms(1)).thenReturn(rooms);

        ExpositionService expoService = new ExpositionService(expositionDAO, scheduleDAO, roomDAO);
        Exposition testExpo = expoService.getExpositionDetails(1, "uk");
        Assert.assertEquals(schedules, testExpo.getSchedule());
        Assert.assertEquals(rooms, testExpo.getRooms());
        Assert.assertEquals("ukr", testExpo.getExpoTheme());
        Assert.assertEquals("опис", testExpo.getDescription());
    }
}