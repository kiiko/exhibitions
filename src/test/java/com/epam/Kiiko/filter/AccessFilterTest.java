package com.epam.Kiiko.filter;

import com.epam.Kiiko.entity.User;
import com.epam.Kiiko.entity.UserRole;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

public class AccessFilterTest {
    private static final String ERROR_PAGE = "/WEB-INF/jsp/common/error.jsp";
    private static final String ACCESS_NOT_ALLOWED = "You are not allowed to use current request";
    private static AccessFilter accessFilter;

    @BeforeClass
    public static void initFilter(){
        FilterConfig filterConfig = mock(FilterConfig.class);
        when((filterConfig.getInitParameter("admin"))).thenReturn("/admin");
        when((filterConfig.getInitParameter("user"))).thenReturn("/user");
        when((filterConfig.getInitParameter("common"))).thenReturn("/common");
        when((filterConfig.getInitParameter("outOfControl"))).thenReturn("/outOfControl");

        accessFilter = new AccessFilter();
        accessFilter.init(filterConfig);
    }

    @Test
    public void verifyUnregisteredUserHasNoAccessToAdminPage() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ServletResponse response = mock(ServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getRequestURI()).thenReturn("/Exhibitions/admin");

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("user")).thenReturn(null);
        when(request.getSession()).thenReturn(session);

        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(request.getRequestDispatcher(ERROR_PAGE)).thenReturn(requestDispatcher);

        accessFilter.doFilter(request, response, filterChain);
        verify(request, times(1)).getRequestURI();
        verify(request, times(1)).getSession();
        verify(request).setAttribute("errorMessage", ACCESS_NOT_ALLOWED);
        verify(request).getRequestDispatcher(ERROR_PAGE);
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void verifyUserHasNoAccessToAdminPage() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ServletResponse response = mock(ServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getRequestURI()).thenReturn("/Exhibitions/admin");

        HttpSession session = mock(HttpSession.class);
        User user = new User.UserBuilder()
                .withRole(UserRole.USER)
                .build();
        when(session.getAttribute("user")).thenReturn(user);
        when(request.getSession()).thenReturn(session);

        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(request.getRequestDispatcher(ERROR_PAGE)).thenReturn(requestDispatcher);

        accessFilter.doFilter(request, response, filterChain);
        verify(request, times(1)).getRequestURI();
        verify(request, times(1)).getSession();

        verify(request).setAttribute("errorMessage", ACCESS_NOT_ALLOWED);
        verify(request).getRequestDispatcher(ERROR_PAGE);
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void verifyUserHasAccessToUserPage() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ServletResponse response = mock(ServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getRequestURI()).thenReturn("/Exhibitions/user");

        HttpSession session = mock(HttpSession.class);
        User user = new User.UserBuilder()
                .withRole(UserRole.USER)
                .build();
        when(session.getAttribute("user")).thenReturn(user);
        when(request.getSession()).thenReturn(session);

        accessFilter.doFilter(request, response, filterChain);
        verify(request, times(1)).getRequestURI();
        verify(request, times(1)).getSession();
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void verifyAdminHasAccessToAdminPage() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ServletResponse response = mock(ServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getRequestURI()).thenReturn("/Exhibitions/admin");

        HttpSession session = mock(HttpSession.class);
        User user = new User.UserBuilder()
                .withRole(UserRole.ADMIN)
                .build();
        when(session.getAttribute("user")).thenReturn(user);
        when(request.getSession()).thenReturn(session);

        accessFilter.doFilter(request, response, filterChain);
        verify(request, times(1)).getRequestURI();
        verify(request, times(1)).getSession();
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void verifyUnregisteredUserHasAccessToCommonPage() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ServletResponse response = mock(ServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getRequestURI()).thenReturn("/Exhibitions/common");

        accessFilter.doFilter(request, response, filterChain);
        verify(request, times(1)).getRequestURI();
        verify(request, times(0)).getSession();
        verify(filterChain).doFilter(request, response);
    }

    @AfterClass
    public static void destroyFilter() {
        accessFilter.destroy();
    }
}