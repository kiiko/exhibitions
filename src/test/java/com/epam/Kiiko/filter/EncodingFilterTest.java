package com.epam.Kiiko.filter;

import org.junit.Test;

import javax.servlet.*;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class EncodingFilterTest {

    @Test
    public void verifyEncodingIsSetAsInConfig() throws ServletException, IOException {
        EncodingFilter encodingFilter = new EncodingFilter();
        FilterConfig filterConfig = mock(FilterConfig.class);
        when(filterConfig.getInitParameter("encoding")).thenReturn("ISO-8859-1");

        encodingFilter.init(filterConfig);
        verify(filterConfig, times(1)).getInitParameter("encoding");

        ServletResponse response = mock(ServletResponse.class);
        ServletRequest request = mock(ServletRequest.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getCharacterEncoding()).thenReturn(null);
        encodingFilter.doFilter(request, response, filterChain);
        verify(request, times(3)).getCharacterEncoding();
        verify(request, times(1)).setCharacterEncoding("ISO-8859-1");
        verify(response, times(1)).setCharacterEncoding("ISO-8859-1");
        verify(filterChain).doFilter(request, response);
        encodingFilter.destroy();
    }

    @Test
    public void verifyDefaultEncodingIsSetIfConfigEmpty() throws ServletException, IOException {
        EncodingFilter encodingFilter = new EncodingFilter();
        FilterConfig filterConfig = mock(FilterConfig.class);
        when(filterConfig.getInitParameter("encoding")).thenReturn(null);

        encodingFilter.init(filterConfig);
        verify(filterConfig, times(1)).getInitParameter("encoding");

        ServletResponse response = mock(ServletResponse.class);
        ServletRequest request = mock(ServletRequest.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getCharacterEncoding()).thenReturn(null);
        encodingFilter.doFilter(request, response, filterChain);
        verify(request, times(3)).getCharacterEncoding();
        verify(request, times(1)).setCharacterEncoding("UTF-8");
        verify(response, times(1)).setCharacterEncoding("UTF-8");
        verify(filterChain).doFilter(request, response);
        encodingFilter.destroy();
    }
}