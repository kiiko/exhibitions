package com.epam.Kiiko.filter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.mockito.Mockito.*;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.io.IOException;

public class SessionLocaleFilterTest {
    private static SessionLocaleFilter sessionLocaleFilter;
    private static final String DEFAULT_LANGUAGE = "en";

    @BeforeClass
    public static void initFilter() throws ServletException {
        FilterConfig filterConfig = mock(FilterConfig.class);
        when(filterConfig.getInitParameter("languages")).thenReturn("en uk");
        when(filterConfig.getInitParameter("default-language")).thenReturn(DEFAULT_LANGUAGE);
        sessionLocaleFilter = new SessionLocaleFilter();
        sessionLocaleFilter.init(filterConfig);
    }

    @Test
    public void verifyDefaultSessionLanguageWillBeSet() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ServletResponse response = mock(ServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("locale")).thenReturn(null);
        when(request.getSession()).thenReturn(session);

        when(request.getParameter("language")).thenReturn(null);

        sessionLocaleFilter.doFilter(request, response, filterChain);
        verify(session, times(1)).getAttribute("locale");
        verify(session, times(1)).setAttribute("locale", DEFAULT_LANGUAGE);
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void verifySessionLanguageWillBeChanged() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ServletResponse response = mock(ServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("locale")).thenReturn("ru");
        when(request.getSession()).thenReturn(session);

        when(request.getParameter("language")).thenReturn("uk");

        sessionLocaleFilter.doFilter(request, response, filterChain);
        verify(session, times(1)).getAttribute("locale");
        verify(session, times(1)).setAttribute("locale", "uk");

        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void verifySessionLanguageWillBeSetToDefaultIfInvalidLanguage() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ServletResponse response = mock(ServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("locale")).thenReturn("uk");
        when(request.getSession()).thenReturn(session);

        when(request.getParameter("language")).thenReturn("ru");

        sessionLocaleFilter.doFilter(request, response, filterChain);
        verify(session, times(1)).setAttribute("locale", DEFAULT_LANGUAGE);

        verify(filterChain).doFilter(request, response);
    }

    @AfterClass
    public static void destroyFilter() throws ServletException {
        sessionLocaleFilter.destroy();
    }
}