package com.epam.Kiiko.dao;

import com.epam.Kiiko.entity.Ticket;
import com.epam.Kiiko.entity.TicketStatus;
import com.epam.Kiiko.exception.DatabaseException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TicketDAO {
    // Class logger
    private static final Logger LOGGER = Logger.getLogger(TicketDAO.class);
    private DBManager dbManager;

    private static final String GET_TICKET_EN = "SELECT ticket_id, ticket.expo_id as expo_id, payment_status, visitors_amount, " +
            "for_date, price, expo.theme_en as theme FROM ticket INNER JOIN exposition expo on ticket.expo_id = expo.expo_id WHERE ticket_id =(?);";
    private static final String GET_TICKET_UA = "SELECT ticket_id, ticket.expo_id as expo_id, payment_status, visitors_amount, " +
            "for_date, price, expo.theme_ua as theme FROM ticket INNER JOIN exposition expo on ticket.expo_id = expo.expo_id WHERE ticket_id =(?);";
    private static final String GET_USER_TICKETS_EN = "SELECT ticket_id, ticket.expo_id as expo_id, payment_status, visitors_amount, " +
            "for_date, price, expo.theme_en as theme FROM ticket INNER JOIN exposition expo on ticket.expo_id = expo.expo_id WHERE user_id =(?);";
    private static final String GET_USER_TICKETS_UA = "SELECT ticket_id, ticket.expo_id as expo_id, payment_status, visitors_amount, " +
            "for_date, price, expo.theme_ua as theme FROM ticket INNER JOIN exposition expo on ticket.expo_id = expo.expo_id WHERE user_id =(?);";
    private static final String ADD_TICKET = "INSERT INTO ticket(user_id, expo_id, visitors_amount, for_date) VALUES " +
            " (?, ?, ?, ?);";
    private static final String UPDATE_TICKET_STATUS = "UPDATE ticket SET payment_status='paid' WHERE ticket_id=(?);";
    private static final String CHECK_IF_TICKET_BELONGS_TO_USER = "SELECT ticket_id FROM ticket WHERE ticket_id=(?)" +
            " AND user_id=(?);";
    private static final String CHECK_IF_TICKET_IS_PAID = "SELECT ticket_id FROM ticket WHERE ticket_id=(?)" +
            " AND payment_status='paid';";

    private static final String DELETE_TICKET = "DELETE FROM ticket WHERE ticket_id=(?);";

    public TicketDAO() {
        dbManager = DBManager.getInstance();
    }

    public TicketDAO(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    /**
     * Return all tickets, which belongs to user.
     */
    public List<Ticket> getUsersTickets(int userId, String locale) throws DatabaseException {
        List<Ticket> userTickets = new ArrayList<>();

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dbManager.getConnection();
            if (locale.equals("uk")) {
                statement = connection.prepareStatement(GET_USER_TICKETS_UA);
            } else {
                statement = connection.prepareStatement(GET_USER_TICKETS_EN);
            }
            statement.setInt(1, userId);

            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                userTickets.add(retrieveUserTicket(resultSet));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }

        return userTickets;
    }

    /**
     * Return ticket object with specific id.
     */
    public Ticket getTicket(int ticketId, String locale) throws DatabaseException {
        Ticket ticket = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dbManager.getConnection();
            if (locale.equals("uk")) {
                statement = connection.prepareStatement(GET_TICKET_UA);
            } else {
                statement = connection.prepareStatement(GET_TICKET_EN);
            }
            statement.setInt(1, ticketId);

            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                ticket = retrieveUserTicket(resultSet);
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }

        return ticket;
    }

    public int createTicket(int userId, int expoId, int visitorsAmount,
                            LocalDate visitingDate) throws DatabaseException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        int newTicketId = -1;

        try {
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(ADD_TICKET, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            statement.setInt(i++, userId);
            statement.setInt(i++, expoId);
            statement.setInt(i++, visitorsAmount);
            statement.setDate(i, Date.valueOf(visitingDate));
            statement.executeUpdate();

            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                newTicketId = resultSet.getInt(1);
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }

        LOGGER.debug("Ticket " + newTicketId + " has been created successfully.");

        return newTicketId;
    }

    /**
     * Set ticket status as 'paid' after successful payment.
     */
    public void changeTicketStatus(int ticketId) throws DatabaseException {
        LOGGER.debug("Started changing ticket status.");
        try (Connection connection = dbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_TICKET_STATUS)) {
            statement.setInt(1, ticketId);
            statement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        }
        LOGGER.debug("Status in ticket " + ticketId + " has been changed successfully.");
    }

    /**
     * Verify that ticket belongs to user. (Used before deleting and paying).
     */
    public boolean ticketBelongToUser(int ticketId, int userId) throws DatabaseException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        boolean ticketBelongs = false;

        try {
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(CHECK_IF_TICKET_BELONGS_TO_USER);
            statement.setInt(1, ticketId);
            statement.setInt(2, userId);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                ticketBelongs = true;
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }

        return ticketBelongs;
    }

    /**
     * Verify if ticket is already paid.
     * */
    public boolean isAlreadyPaid(int ticketId) throws DatabaseException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        boolean ticketIsPayed = false;

        try {
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(CHECK_IF_TICKET_IS_PAID);
            statement.setInt(1, ticketId);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                ticketIsPayed = true;
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }

        return ticketIsPayed;
    }

    public void deleteTicket(int ticketId) throws DatabaseException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(DELETE_TICKET);
            statement.setInt(1, ticketId);
            statement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(statement);
            DBManager.close(connection);
        }
    }

    private Ticket retrieveUserTicket(ResultSet resultSet) throws SQLException {
        return new Ticket.Builder()
                .withExpoId(resultSet.getInt("expo_id"))
                .withExpoTheme(resultSet.getString("theme"))
                .withPrice(resultSet.getDouble("price"))
                .withTicketId(resultSet.getInt("ticket_id"))
                .withTicketStatus(TicketStatus.valueOf(resultSet.getString("payment_status").toUpperCase()))
                .withVisitingDate(resultSet.getDate("for_date").toLocalDate())
                .withVisitorsAmount(resultSet.getInt("visitors_amount"))
                .build();
    }
}
