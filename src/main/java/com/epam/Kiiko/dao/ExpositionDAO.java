package com.epam.Kiiko.dao;

import com.epam.Kiiko.entity.Exposition;
import com.epam.Kiiko.entity.UserRole;
import com.epam.Kiiko.exception.DatabaseException;
import com.epam.Kiiko.service.ExpoSearchDetails;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * DAO method to work only with Exposition table.
 */
public class ExpositionDAO {
    // Class logger
    private static final Logger LOGGER = Logger.getLogger(ExpositionDAO.class);

    private DBManager dbManager;

    //////////////////////////////////////
    // Field names in DB tables exposition
    //////////////////////////////////////
    private static final String DB_EXPO_ID = "exposition.expo_id";
    private static final String DB_EXPO_TICKET_PRICE = "ticket_price";
    private static final String DB_EXPO_THEME_UA = "theme_ua";
    private static final String DB_EXPO_THEME_EN = "theme_en";
    private static final String DB_EXPO_DESCRIPTION_UA = "description_ua";
    private static final String DB_EXPO_DESCRIPTION_EN = "description_en";

    //////////////////////////////////////
    // Methods
    //////////////////////////////////////
    private static final String METHOD_GET_LIST = "get_list";
    private static final String METHOD_GET_COUNT = "get_count";

    //////////////////////////////////////////////////////
    // SQL queries for obtaining expositions from database
    //////////////////////////////////////////////////////
    private static final String GET_COUNT_OF_EXPOSITIONS = "SELECT DISTINCT COUNT(*) as count FROM exposition";

    private static final String GET_EXPOSITION = "SELECT DISTINCT exposition.expo_id, ticket_price, theme_en, theme_ua, " +
            "description_en, description_ua FROM exposition ";

    private static final String INNER_JOIN_TIMETABLE = " INNER JOIN exposition_schedule AS expo_shd ON exposition.expo_id=expo_shd.expo_id";

    private static final String FIND_EXPO_BY_ID = " WHERE expo_id=?;";
    private static final String FIND_EXPO_DEFAULT_ADMIN = INNER_JOIN_TIMETABLE + " ORDER BY expo_shd.start_day ASC";
    private static final String FIND_EXPO_DEFAULT_USER = INNER_JOIN_TIMETABLE + " WHERE end_day>=DATE(NOW()) ORDER BY expo_shd.start_day ASC";

    private static final String FIND_EXPO_BY_PRICE_ADMIN = " WHERE ticket_price>=? AND ticket_price<=? ORDER BY ticket_price ASC";
    private static final String FIND_EXPO_BY_PRICE_USER = INNER_JOIN_TIMETABLE + " WHERE end_day>=DATE(NOW()) AND ticket_price>=? AND ticket_price<=? ORDER BY ticket_price ASC";

    private static final String FIND_EXPO_BY_THEME_ADMIN = " WHERE THEME_WITH_LOCALE LIKE ?";
    private static final String FIND_EXPO_BY_THEME_USER = INNER_JOIN_TIMETABLE + " WHERE end_day>=DATE(NOW()) AND THEME_WITH_LOCALE LIKE ?";

    private static final String FIND_EXPO_BY_DATE_ADMIN = INNER_JOIN_TIMETABLE + " WHERE start_day BETWEEN ? AND ? " +
            "OR end_day BETWEEN ? AND ? OR (start_day <= ? AND end_day >= ?) ORDER BY expo_shd.start_day ASC";
    private static final String FIND_EXPO_BY_DATE_USER = INNER_JOIN_TIMETABLE + " WHERE end_day>=DATE(NOW()) AND start_day " +
            "BETWEEN ? AND ? OR end_day BETWEEN ? AND ? OR (start_day <= ? AND end_day >= ?) ORDER BY expo_shd.start_day ASC";

    private static final String LIMIT_EXPO = " LIMIT ?, ?;";

    private static final String CHECK_EXPO_EXIST = "SELECT expo_id FROM exposition WHERE expo_id=(?);";

    private static final String ADD_EXPOSITION = "INSERT INTO exposition (ticket_price, theme_ua, theme_en," +
            "description_ua, description_en) VALUES (?, ?, ?, ?,?);";

    private static final String UPDATE_EXPOSITION = "UPDATE exposition SET ticket_price=(?), theme_ua=(?), theme_en=(?)," +
            " description_ua=(?), description_en=(?) WHERE expo_id=(?);";
    private static final String DELETE_EXPOSITION = "DELETE FROM exposition WHERE expo_id=(?);";

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    /**
     * Default constructor
     * */
    public ExpositionDAO() {
        dbManager = DBManager.getInstance();
    }

    /**
     * Constructor for custom DBManager objects
     *
     * @param dbManager DBManager object with configured DataSource
     * */
    public ExpositionDAO(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    public Exposition retrieveExposition(ResultSet resultSet) throws SQLException {
        return new Exposition.Builder()
                .withExpoId(resultSet.getInt(DB_EXPO_ID))
                .withPrice(resultSet.getDouble(DB_EXPO_TICKET_PRICE))
                .withExpoThemeEn(resultSet.getString(DB_EXPO_THEME_EN))
                .withExpoThemeUa(resultSet.getString(DB_EXPO_THEME_UA))
                .withDescriptionEn(resultSet.getString(DB_EXPO_DESCRIPTION_EN))
                .withDescriptionUa(resultSet.getString(DB_EXPO_DESCRIPTION_UA))
                .build();
    }

    /**
     * Current method sets values into PreparedStatement depends on method and search criteria.
     */
    protected PreparedStatement setValuesForStatement(PreparedStatement statement, String stmtForMethod,
                                                      ExpoSearchDetails searchDetails) throws SQLException {
        String searchBy = searchDetails.getSearchBy();
        int start = searchDetails.getFirstRow();
        int rowsCount = searchDetails.getRowsCount();

        int i = 1;

        switch (searchBy) {
            case ExpoSearchDetails.SEARCH_BY_PRICE:
                statement.setDouble(i++, searchDetails.getPriceFrom());
                statement.setDouble(i++, searchDetails.getPriceTo());
                break;
            case ExpoSearchDetails.SEARCH_BY_THEME:
                statement.setString(i++, "%" + searchDetails.getTheme() + "%");
                break;
            case ExpoSearchDetails.SEARCH_BY_DATE:
                int dateInsertions = 3;
                for (int j = 0; j < dateInsertions; j++) {
                    statement.setDate(i++, Date.valueOf(searchDetails.getDateFrom()));
                    statement.setDate(i++, Date.valueOf(searchDetails.getDateTo()));
                }
                break;
            case ExpoSearchDetails.SEARCH_BY_DEFAULT:
                break;
        }
        if (stmtForMethod.equals(METHOD_GET_LIST)) {
            statement.setInt(i++, start);
            statement.setInt(i, rowsCount);
        }
        return statement;
    }

    /**
     * Current method create Prepared statement for selecting count of  Expositions in database depends on UserRole
     * and parameters. Params Map contains pairs with param key (e.g. first row to be selected or price to find by) and
     * its value.
     *
     * @return preparedStatement - is ready to execute query (all parameters is set).
     */
    protected PreparedStatement createStatement(Connection connection, String stmtForMethod,
                                                ExpoSearchDetails searchDetails) throws SQLException {
        LOGGER.debug("Creating statement for selecting expositions");
        LOGGER.debug("SearchDetails " + searchDetails);

        String searchBy = searchDetails.getSearchBy();
        String locale = searchDetails.getLocale();
        UserRole userRole = searchDetails.getUserRole();

        String sqlQuery = "";

        // At first choose searching criteria (SQL query depends on it).
        // Then choose if statement is for getting exposition list or exposition count.
        // Also, if user role is ADMIN, selection is provided from all records in Database.
        // Otherwise, selection is provided only from active at the request time expositions.
        switch (searchBy) {
            case ExpoSearchDetails.SEARCH_BY_PRICE:
                if (stmtForMethod.equals(METHOD_GET_LIST)) {
                    sqlQuery = GET_EXPOSITION;
                } else {
                    sqlQuery = GET_COUNT_OF_EXPOSITIONS;
                }
                sqlQuery += (userRole == UserRole.ADMIN) ? FIND_EXPO_BY_PRICE_ADMIN : FIND_EXPO_BY_PRICE_USER;
                break;
            case ExpoSearchDetails.SEARCH_BY_THEME:
                if (stmtForMethod.equals(METHOD_GET_LIST)) {
                    sqlQuery = GET_EXPOSITION;
                } else {
                    sqlQuery = GET_COUNT_OF_EXPOSITIONS;
                }
                sqlQuery += (userRole == UserRole.ADMIN) ? FIND_EXPO_BY_THEME_ADMIN : FIND_EXPO_BY_THEME_USER;
                if (locale.equals("uk")) {
                    sqlQuery = sqlQuery.replace("THEME_WITH_LOCALE", DB_EXPO_THEME_UA);
                } else {
                    sqlQuery = sqlQuery.replace("THEME_WITH_LOCALE", DB_EXPO_THEME_EN);
                }
                break;
            case ExpoSearchDetails.SEARCH_BY_DATE:
                if (stmtForMethod.equals(METHOD_GET_LIST)) {
                    sqlQuery = GET_EXPOSITION;
                } else {
                    sqlQuery = GET_COUNT_OF_EXPOSITIONS;
                }
                sqlQuery += (userRole == UserRole.ADMIN) ? FIND_EXPO_BY_DATE_ADMIN : FIND_EXPO_BY_DATE_USER;
                break;
            case ExpoSearchDetails.SEARCH_BY_DEFAULT:
                if (stmtForMethod.equals(METHOD_GET_LIST)) {
                    sqlQuery = GET_EXPOSITION;
                } else {
                    sqlQuery = GET_COUNT_OF_EXPOSITIONS;
                }
                sqlQuery += (userRole == UserRole.ADMIN) ? FIND_EXPO_DEFAULT_ADMIN : FIND_EXPO_DEFAULT_USER;
                break;
        }
        if (stmtForMethod.equals(METHOD_GET_LIST)) {
            sqlQuery += LIMIT_EXPO;
        } else {
            sqlQuery += ";";
        }

        LOGGER.debug(sqlQuery);
        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        statement = setValuesForStatement(statement, stmtForMethod, searchDetails);
        LOGGER.debug(statement.toString());

        return statement;
    }

    /**
     * Current method gets Exposition list from database. SearchDetails object contains additional information for searching.
     * @see ExpoSearchDetails ExpoSearchDetails.class
     */
    public List<Exposition> getExpoList(ExpoSearchDetails searchDetails) throws DatabaseException {
        List<Exposition> expositionList = new ArrayList<>();

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dbManager.getConnection();
            statement = createStatement(connection, METHOD_GET_LIST, searchDetails);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                expositionList.add(retrieveExposition(resultSet));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }

        return expositionList;
    }

    /**
     * Return count of expositions in database based on search criteria
     */
    public int getExpoCount(ExpoSearchDetails searchDetails) throws DatabaseException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        int count = 0;
        try {
            connection = dbManager.getConnection();
            statement = createStatement(connection, METHOD_GET_COUNT, searchDetails);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt("count");
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }
        return count;
    }

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    public Exposition getExpositionById(int expoId) {
        Exposition exposition = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        String sqlQuery = GET_EXPOSITION + FIND_EXPO_BY_ID;

        try {
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, expoId);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                exposition = retrieveExposition(resultSet);
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }

        return exposition;
    }

    public int addExposition(double ticketPrice, Map<String, String> textInfo, Connection connection) throws SQLException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int newExpoId;
        try {
            LOGGER.debug("Starting adding exposition...");
            statement = connection.prepareStatement(ADD_EXPOSITION, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            statement.setDouble(i++, ticketPrice);
            statement.setString(i++, textInfo.get("themeUA"));
            statement.setString(i++, textInfo.get("themeEN"));
            statement.setString(i++, textInfo.get("descriptionUA"));
            statement.setString(i, textInfo.get("descriptionEN"));

            statement.executeUpdate();

            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                newExpoId = resultSet.getInt(1);
                LOGGER.debug("Exposition text info added successfully");
            } else {
                LOGGER.debug("Exposition text info hasn't been added");
                throw new SQLException();
            }

        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new SQLException();
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
        }
        return newExpoId;
    }

    public void deleteExposition(int expoId) throws DatabaseException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            LOGGER.debug("Starting deleting exposition...");
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(DELETE_EXPOSITION);

            statement.setInt(1, expoId);
            statement.executeUpdate();
            LOGGER.debug("Exposition deleted successfully");
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(statement);
            DBManager.close(connection);
        }
    }

    /**
     * Check if exposition with current id exists in database
     */
    public boolean ifExpositionExist(int expoId) throws DatabaseException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        boolean expoExists = false;

        try {
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(CHECK_EXPO_EXIST);
            statement.setInt(1, expoId);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                expoExists = true;
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }

        return expoExists;
    }

    /**
     * Updating exposition information in exposition table.
     * First step in transactional exposition updating.
     *
     * @param connection Connection object for transaction
     * @param exposition Exposition object with new info
     */
    public void updateExposition(Connection connection, Exposition exposition) throws DatabaseException {
        LOGGER.debug("Starting updating exposition...");
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_EXPOSITION);
            int i = 1;
            statement.setDouble(i++, exposition.getPrice());
            statement.setString(i++, exposition.getExpoThemeUa());
            statement.setString(i++, exposition.getExpoThemeEn());
            statement.setString(i++, exposition.getDescriptionUa());
            statement.setString(i++, exposition.getDescriptionEn());
            statement.setInt(i, exposition.getExpoId());
            statement.executeUpdate();

            LOGGER.debug("Exposition has been successfully updated.");
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(statement);
        }
    }
}
