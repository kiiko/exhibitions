package com.epam.Kiiko.dao;

import com.epam.Kiiko.entity.ExpositionSchedule;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ExpositionScheduleDAO {
    private static final Logger LOGGER = Logger.getLogger(ExpositionScheduleDAO.class);

    private DBManager dbManager;

    //////////////
    // Field names in DB tables exposition_schedule
    //////////////
    private static final String DB_EXPO_SCHEDULE_ID = "schedule_id";
    private static final String DB_EXPO_SCHEDULE_START_TIME = "start_time";
    private static final String DB_EXPO_SCHEDULE_END_TIME = "end_time";
    private static final String DB_EXPO_SCHEDULE_START_DATE = "start_day";
    private static final String DB_EXPO_SCHEDULE_END_DATE = "end_day";

    //////////////
    // SQL queries
    //////////////
    private static final String GET_SCHEDULE_FOR_EXPOSITION = "SELECT * FROM exposition_schedule WHERE expo_id=(?);";
    private static final String SET_SCHEDULE_FOR_EXPOSITION = "INSERT INTO exposition_schedule(expo_id, start_time, end_time, " +
            "start_day, end_day) VALUES (?, ?, ?, ?, ?);";
    private static final String DELETE_SCHEDULE_FOR_EXPOSITION = "DELETE FROM exposition_schedule WHERE expo_id=(?);";

    ////////////////
    // Constructors
    ////////////////

    public ExpositionScheduleDAO() {
        dbManager = DBManager.getInstance();
    }

    public ExpositionScheduleDAO(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    //////////////
    // Methods
    //////////////

    public ExpositionSchedule retrieveSchedule(ResultSet resultSet) throws SQLException {
        ExpositionSchedule expositionSchedule = new ExpositionSchedule();

        expositionSchedule.setScheduleId(resultSet.getInt(DB_EXPO_SCHEDULE_ID));
        expositionSchedule.setStartDay(resultSet.getDate(DB_EXPO_SCHEDULE_START_DATE).toLocalDate());
        expositionSchedule.setEndDay(resultSet.getDate(DB_EXPO_SCHEDULE_END_DATE).toLocalDate());
        expositionSchedule.setStartTime(resultSet.getTime(DB_EXPO_SCHEDULE_START_TIME).toLocalTime());
        expositionSchedule.setEndTime(resultSet.getTime(DB_EXPO_SCHEDULE_END_TIME).toLocalTime());

        return expositionSchedule;
    }

    /**
     * Find schedule for specific exposition.
     *
     * @param expoID exposition ID
     * @return List of ExpositionSchedule objects
     */
    public List<ExpositionSchedule> getScheduleForExposition(int expoID) {
        List<ExpositionSchedule> expoSchedule = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(GET_SCHEDULE_FOR_EXPOSITION);
            statement.setInt(1, expoID);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                expoSchedule.add(retrieveSchedule(resultSet));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }
        return expoSchedule;
    }

    /**
     * Current method is a part of transaction - set schedule for new exposition
     *
     * @param connection Connection from main method
     * @param schedule   Exposition schedule object with all information
     * @param expoId     Exposition id
     */
    public void setScheduleForExposition(int expoId, ExpositionSchedule schedule, Connection connection) throws SQLException {
        LOGGER.debug("Setting schedule " + schedule + " for expo " + expoId);
        try (PreparedStatement statement = connection.prepareStatement(SET_SCHEDULE_FOR_EXPOSITION)) {
            int i = 1;
            statement.setInt(i++, expoId);
            statement.setTime(i++, Time.valueOf(schedule.getStartTime()));
            statement.setTime(i++, Time.valueOf(schedule.getEndTime()));
            statement.setDate(i++, Date.valueOf(schedule.getStartDay()));
            statement.setDate(i, Date.valueOf(schedule.getEndDay()));
            statement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new SQLException(ex);
        }
        LOGGER.debug("Set successfully");
    }

    /**
     * Delete all schedule records for selected exposition.
     *
     * @param connection Connection from main method
     * @param expoId     Exposition id
     * */
    public void deleteScheduleForExposition(Connection connection, int expoId) throws SQLException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(DELETE_SCHEDULE_FOR_EXPOSITION);
            statement.setInt(1, expoId);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new SQLException();
        }
    }
}
