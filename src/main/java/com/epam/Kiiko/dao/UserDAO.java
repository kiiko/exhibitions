package com.epam.Kiiko.dao;

import com.epam.Kiiko.entity.User;
import com.epam.Kiiko.entity.UserRole;
import com.epam.Kiiko.exception.DatabaseException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO {
    // Class logger
    private static final Logger LOGGER = Logger.getLogger(UserDAO.class);
    private DBManager dbManager;

    ///////////////////////////////////////
    // Field names in DB table user_account
    ///////////////////////////////////////
    private static final String DB_USER_ID = "user_id";
    private static final String DB_USER_LOGIN = "login";
    private static final String DB_USER_PASSWORD = "passw";
    private static final String DB_USER_ROLE = "user_role";
    private static final String DB_USER_NAME = "name";
    private static final String DB_USER_SURNAME = "surname";
    private static final String DB_USER_EMAIL = "email";

    ////////////////////////////////////////////////////
    // Constants - SQL queries - for Statement executing
    ////////////////////////////////////////////////////
    private static final String FIND_USER_WITH_LOGIN_OR_EMAIL = "SELECT * FROM user_account WHERE login=(?) OR email=(?);";
    private static final String FIND_USER_BY_ID = "SELECT * FROM user_account WHERE user_id=(?);";
    private static final String CHECK_USER_EMAIL_AND_LOGIN = "SELECT user_id FROM user_account " +
            "WHERE email=(?) OR login=(?);";
    private static final String ADD_USER = "INSERT INTO user_account (email, login, passw, user_role, name, surname) " +
            "VALUES (?, ?, ?, ?, ?, ?);";

    ////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////

    public UserDAO() {
        dbManager = DBManager.getInstance();
    }

    public UserDAO(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    ////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////

    /**
     * Method retrieve User from ResultSet
     *
     * @param resSet Result Set with rows with values from user_account table in database
     * @return User object
     */
    private User retrieveUser(ResultSet resSet) throws SQLException {
        int user_id = resSet.getInt(DB_USER_ID);
        String login = resSet.getString(DB_USER_LOGIN);
        String password = resSet.getString(DB_USER_PASSWORD);
        String name = resSet.getString(DB_USER_NAME);
        String surname = resSet.getString(DB_USER_SURNAME);
        String email = resSet.getString(DB_USER_EMAIL);
        String role = resSet.getString(DB_USER_ROLE);

        return new User.UserBuilder()
                .withId(user_id)
                .withLogin(login)
                .withPassword(password)
                .withName(name)
                .withSurname(surname)
                .withEmail(email)
                .withRole(UserRole.valueOf(role.toUpperCase()))
                .build();
    }

    /**
     * Method returns User object from Database with input ID.
     * */
    public User getUserById(int id) throws DatabaseException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(FIND_USER_BY_ID);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                user = retrieveUser(resultSet);
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(connection);
            DBManager.close(statement);
            DBManager.close(resultSet);
        }
        return user;
    }

    /**
     * Method find in database for User with @login
     *
     * @param login String value of user login
     * @return User object
     */
    public User getUserByLoginOrEmail(String login) throws DatabaseException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(FIND_USER_WITH_LOGIN_OR_EMAIL);
            statement.setString(1, login);
            statement.setString(2, login);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                user = retrieveUser(resultSet);
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(connection);
            DBManager.close(statement);
            DBManager.close(resultSet);
        }
        return user;
    }

    /**
     * Add user to database.
     *
     * @param user Object User, which should be stored in database
     */
    public void addUser(User user) throws DatabaseException {
        LOGGER.debug("Start adding user");
        LOGGER.debug("User to be added " + user);
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(ADD_USER);

            statement.setString(1, user.getEmail());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getRole().toString().toLowerCase());
            statement.setString(5, user.getName());
            statement.setString(6, user.getSurname());

            LOGGER.debug("Statement for adding user " + statement);

            statement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException("Can't add user");
        } finally {
            DBManager.close(statement);
            DBManager.close(connection);
        }
        LOGGER.debug("User added");
    }

    /**
     * Check if user with email and login already exist in database
     *
     * @param email email string, which user tries to use while going through registration
     * @param login login string, which user tries to use while going through registration
     * @return {@code true} - email and login both can be used for registration,
     * {@code false} - email or login, or both can't be used for registration
     */
    public boolean areAvailableEmailAndLogin(String email, String login) throws DatabaseException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        boolean valuesAreAvailable = true;

        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(CHECK_USER_EMAIL_AND_LOGIN);
            statement.setString(1, email);
            statement.setString(2, login);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                valuesAreAvailable = false;
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new DatabaseException();
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }
        return valuesAreAvailable;
    }

}
