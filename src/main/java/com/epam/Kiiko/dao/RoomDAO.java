package com.epam.Kiiko.dao;

import com.epam.Kiiko.entity.Room;
import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RoomDAO{
    private static final Logger LOGGER = Logger.getLogger(RoomDAO.class);
    private DBManager dbManager;

    ///////////////////////////////
    // Field names in DB table Room
    ///////////////////////////////
    private static final String DB_ROOM_ID = "room_id";
    private static final String DB_ROOM_NUMBER = "room_num";
    private static final String DB_ROOM_FLOOR = "floor";

    // SQL queries
    private static final String GET_AVAILABLE_ROOMS = "CALL GetAvailableRooms(?, ?);";
    private static final String GET_AVAILABLE_ROOMS_FOR_EDITING = "CALL GetAvailableRoomsForExpoEditing(?, ?, ?);";
    private static final String GET_EXPOSITION_ROOMS = "SELECT room.room_id, floor, room.room_num FROM room " +
            "INNER JOIN exposition_rooms as expo_room ON room.room_id = expo_room.room_id WHERE expo_room.expo_id=?;";
    private static final String SET_ROOMS_FOR_EXPOSITION = "INSERT INTO exposition_rooms(expo_id, room_id) VALUES (?, ?);";
    private static final String DELETE_ROOM_ASSIGNED_TO_EXPO = "DELETE FROM exposition_rooms WHERE expo_id=(?);";

    ///////////////////////////////////
    // Constructors
    //////////////////////////////////

    public RoomDAO() {
        dbManager = DBManager.getInstance();
    }

    public RoomDAO(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    ///////////////////////////////////
    // Methods
    //////////////////////////////////

    private Room retrieveRoom(ResultSet resultSet) throws SQLException {
        Room room = new Room();
        room.setRoomId(resultSet.getInt(DB_ROOM_ID));
        room.setFloor(resultSet.getInt(DB_ROOM_FLOOR));
        room.setRoomNum(resultSet.getInt(DB_ROOM_NUMBER));
        return room;
    }

    /**
     * This method returns all rooms which are available at time, user want to create new exposition.
     *
     * @param dateFrom LocalDate start of time period
     * @param dateTo end of searched time period
     * */
    public List<Room> getAvailableRooms(LocalDate dateFrom, LocalDate dateTo) {
        LOGGER.debug("DateFrom " + dateFrom + " and DateTo " + dateTo);
        List<Room> availableRooms = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(GET_AVAILABLE_ROOMS);
            statement.setDate(1, Date.valueOf(dateFrom));
            statement.setDate(2, Date.valueOf(dateTo));
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                availableRooms.add(retrieveRoom(resultSet));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }
        return availableRooms;
    }

    /**
     * Current method returns all rooms which are available at time, user want to set for exposition, considering old
     * exposition_rooms records for exposition being edited are deleted.
     * @param dateFrom date of periods start
     * @param dateTo  date of periods end
     * @param expoIdEdited ID of exposition being edited
     * */
    public List<Room> getAvailableRoomsForEditing(LocalDate dateFrom, LocalDate dateTo, int expoIdEdited) {
        LOGGER.debug("DateFrom " + dateFrom + " and DateTo " + dateTo + " expo is editing " + expoIdEdited);
        List<Room> availableRooms = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(GET_AVAILABLE_ROOMS_FOR_EDITING);
            statement.setDate(1, Date.valueOf(dateFrom));
            statement.setDate(2, Date.valueOf(dateTo));
            statement.setInt(3, expoIdEdited);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                availableRooms.add(retrieveRoom(resultSet));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }
        return availableRooms;
    }

    /**
     * This method returns all rooms which are assigned to specific exposition.
     * */
    public List<Room> getExpositionRooms(int expoId) {
        List<Room> expoRooms = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dbManager.getConnection();
            statement = connection.prepareStatement(GET_EXPOSITION_ROOMS);
            statement.setInt(1, expoId);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                expoRooms.add(retrieveRoom(resultSet));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
        } finally {
            DBManager.close(resultSet);
            DBManager.close(statement);
            DBManager.close(connection);
        }

        return expoRooms;
    }

    /**
     * This method assigns rooms to selected exposition.
     *
     * @param connection Connection object for transaction
     * @param expoId Exposition id that will be assigned
     * @param roomId Room id that will be assigned
     * */
    public void setRoomsForExposition(int expoId, int roomId, Connection connection) throws SQLException {
        LOGGER.debug("Set room " + roomId + " for exposition " + expoId);
        try (PreparedStatement statement = connection.prepareStatement(SET_ROOMS_FOR_EXPOSITION)) {
            statement.setInt(1, expoId);
            statement.setInt(2, roomId);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new SQLException(e);
        }
    }

    /**
     * Remove all records from exposition_rooms table for current Exposition with expoId
     *
     * @param expoId Exposition id which rooms will be unassigned
     * @param connection Connection object for transaction
     * */
    public void removeRoomsFromExposition(Connection connection, int expoId) throws SQLException {
        LOGGER.debug("Started removing rooms from exposition " + expoId);
        try (PreparedStatement statement = connection.prepareStatement(DELETE_ROOM_ASSIGNED_TO_EXPO)) {
            statement.setInt(1, expoId);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new SQLException();
        }
    }
}
