package com.epam.Kiiko;

public final class PagePath {

    public static final String PAGE_SIGN_IN = "/WEB-INF/jsp/common/sign-in.jsp";
    public static final String PAGE_EXPOSITION_LIST = "/WEB-INF/jsp/common/exhibition-display.jsp";
    public static final String PAGE_REGISTRATION = "/WEB-INF/jsp/common/user-registration.jsp";

    public static final String PAGE_USER_ORDER_TICKET = "/WEB-INF/jsp/user/order-ticket.jsp";
    public static final String PAGE_USER_TICKETS_LIST = "/WEB-INF/jsp/user/tickets-list.jsp";
    public static final String PAGE_USER_PAY_FOR_TICKET = "/WEB-INF/jsp/user/pay-for-ticket.jsp";

    public static final String PAGE_ADMIN_ADD_EXPOSITION = "/WEB-INF/jsp/admin/add-exposition.jsp";
    public static final String PAGE_ADMIN_EDIT_EXPOSITION = "/WEB-INF/jsp/admin/edit-exposition.jsp";

    public static final String PAGE_ERROR = "/WEB-INF/jsp/common/error.jsp";
    public static final String PAGE_MESSAGE = "/WEB-INF/jsp/common/message.jsp";

    private PagePath() { }
}
