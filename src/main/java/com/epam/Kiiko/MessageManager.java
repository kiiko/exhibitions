package com.epam.Kiiko;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessageManager {
    public static final String MESSAGE_ACCESS_NOT_ALLOWED = "message.access_not_allowed";
    public static final String MESSAGE_INVALID_REQUEST_PARAMS = "message.invalid_params_for_request";
    public static final String MESSAGE_DB_EXCEPTION = "message.database_exception";
    public static final String MESSAGE_EMPTY_PARAMS = "message.params_cant_be_empty";
    public static final String MESSAGE_LOGIN_NOT_AVAILABLE = "message.email_or_login_not_available";
    public static final String MESSAGE_WRONG_SIGN_IN = "message.wrong_sign_in_params";
    public static final String MESSAGE_INVALID_DATE = "message.invalid_date_params";
    public static final String MESSAGE_INVALID_START_DATE = "message.start_day_in_past";
    public static final String MESSAGE_NO_EXPO_FOUND = "message.no_exposition_find";
    public static final String MESSAGE_INVALID_EMAIL = "message.invalid_email";

    private static final String MESSAGES_FILE_NAME = "messages";
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(MESSAGES_FILE_NAME, new Locale("", ""));

    private MessageManager() {}

    /**
     * Return message string
     *
     * @param key Key of message in resource bundle
     * */
    public static String getMessage(String key) {
        return resourceBundle.getString(key);
    }

    /**
     * Change language of messages on jsp page.
     *
     * @param language selected language
     * */
    public static void changeLocale(String language) {
        resourceBundle = ResourceBundle.getBundle(MESSAGES_FILE_NAME, new Locale(language));
    }
}
