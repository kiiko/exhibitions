package com.epam.Kiiko.service;

import com.epam.Kiiko.dao.TicketDAO;
import com.epam.Kiiko.exception.DatabaseException;
import org.apache.log4j.Logger;

public class TicketService {
    private static final Logger LOGGER = Logger.getLogger(TicketService.class);
    private final TicketDAO ticketDAO;

    public TicketService() {
        ticketDAO = new TicketDAO();
    }

    public TicketService(TicketDAO ticketDAO) {
        this.ticketDAO = ticketDAO;
    }

    /**
     * Current method check if ticket from request is associated with current user and also check if ticket is already paid
     * */
    public boolean isInvalidTicketStatusForUser(int ticketId, int userId) throws DatabaseException {
        // check if ticket belongs to current user
        // if no, forward user to error page
        if (! ticketDAO.ticketBelongToUser(ticketId, userId)) {
            LOGGER.debug("Access user " + userId + " to ticket " + ticketId + " is denied.");
            return true;
        }

        // check if ticket is already paid
        if (ticketDAO.isAlreadyPaid(ticketId)) {
            LOGGER.debug("Ticket " + ticketId + " is already paid");
            return true;
        }
        return false;
    }
}
