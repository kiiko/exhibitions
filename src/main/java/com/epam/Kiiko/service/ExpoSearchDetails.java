package com.epam.Kiiko.service;

import com.epam.Kiiko.entity.UserRole;

import java.time.LocalDate;

/**
 * Helper class for provide search information for selecting expositions from database
 * */
public class ExpoSearchDetails {
    public static final String SEARCH_BY_DEFAULT = "byDefault";
    public static final String SEARCH_BY_PRICE = "byPrice";
    public static final String SEARCH_BY_THEME = "byTheme";
    public static final String SEARCH_BY_DATE = "byDate";

    private int firstRow;
    private int rowsCount;
    private String locale;
    private String searchBy;
    private UserRole userRole;

    private double priceFrom;
    private double priceTo;

    private String theme;

    private LocalDate dateFrom;
    private LocalDate dateTo;

    public ExpoSearchDetails(String locale, String searchBy, UserRole userRole) {
        this.locale = locale;
        this.searchBy = searchBy;
        this.userRole = userRole;
    }

    public void setRows(int firstRow, int rowsCount) {
        this.firstRow = firstRow;
        this.rowsCount = rowsCount;
    }

    public void setForPriceSearch(double priceFrom, double priceTo) {
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
    }

    public void setForThemeSearch(String theme) {
        this.theme = theme;
    }

    public void setForDateSearch(LocalDate dateFrom, LocalDate dateTo) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public int getFirstRow() {
        return firstRow;
    }

    public int getRowsCount() {
        return rowsCount;
    }

    public String getLocale() {
        return locale;
    }

    public String getSearchBy() {
        return searchBy;
    }

    public double getPriceFrom() {
        return priceFrom;
    }

    public double getPriceTo() {
        return priceTo;
    }

    public String getTheme() {
        return theme;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    @Override
    public String toString() {
        String details = "ExpositionSearchDetails{firstRow=" + firstRow +
                ", rowsCount=" + rowsCount +
                ", locale='" + locale + '\'' +
                ", searchBy='" + searchBy + '\'' +
                ", UserRole='" + userRole + '\'';
        if (searchBy.equals(SEARCH_BY_DEFAULT)) {
            return details + '}';
        } else if (searchBy.equals(SEARCH_BY_PRICE)) {
            return details + ", priceFrom=" + priceFrom + ", priceTo=" + priceTo + '}';
        } else if (searchBy.equals(SEARCH_BY_THEME)) {
            return details + ", theme='" + theme + '\'' + '}';
        }
        return details + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + '}';
    }
}
