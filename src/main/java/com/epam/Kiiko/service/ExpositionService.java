package com.epam.Kiiko.service;

import com.epam.Kiiko.dao.DBManager;
import com.epam.Kiiko.dao.ExpositionDAO;
import com.epam.Kiiko.dao.ExpositionScheduleDAO;
import com.epam.Kiiko.dao.RoomDAO;
import com.epam.Kiiko.entity.Exposition;
import com.epam.Kiiko.entity.ExpositionSchedule;
import com.epam.Kiiko.entity.Room;
import com.epam.Kiiko.exception.DatabaseException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class ExpositionService {
    private static final Logger LOGGER = Logger.getLogger(ExpositionService.class);
    private final ExpositionDAO expositionDAO;
    private final ExpositionScheduleDAO scheduleDAO;
    private final RoomDAO roomDAO;

    public ExpositionService() {
        expositionDAO = new ExpositionDAO();
        scheduleDAO = new ExpositionScheduleDAO();
        roomDAO = new RoomDAO();
    }

    public ExpositionService(ExpositionDAO expositionDAO, ExpositionScheduleDAO scheduleDAO, RoomDAO roomDAO) {
        this.expositionDAO = expositionDAO;
        this.scheduleDAO = scheduleDAO;
        this.roomDAO = roomDAO;
    }

    public List<Exposition> getExpositionList(ExpoSearchDetails searchDetails) throws DatabaseException {
        List<Exposition> expositions = expositionDAO.getExpoList(searchDetails);
        for (Exposition exposition : expositions) {
            if ("uk".equals(searchDetails.getLocale())) {
                exposition.setExpoTheme(exposition.getExpoThemeUa());
                exposition.setDescription(exposition.getDescriptionUa());
            } else {
                exposition.setExpoTheme(exposition.getExpoThemeEn());
                exposition.setDescription(exposition.getDescriptionEn());
            }
            int id = exposition.getExpoId();
            exposition.setSchedule(scheduleDAO.getScheduleForExposition(id));
            exposition.setRooms(roomDAO.getExpositionRooms(id));
        }
        return expositions;
    }

    public int getExpositionCount(ExpoSearchDetails searchDetails) throws DatabaseException {
        return expositionDAO.getExpoCount(searchDetails);
    }

    /**
     * Current method return exposition details
     * */
    public Exposition getExpositionDetails(int expoId, String locale) {
        Exposition exposition = expositionDAO.getExpositionById(expoId);
        if ("uk".equals(locale)) {
            exposition.setExpoTheme(exposition.getExpoThemeUa());
            exposition.setDescription(exposition.getDescriptionUa());
        } else {
            exposition.setExpoTheme(exposition.getExpoThemeEn());
            exposition.setDescription(exposition.getDescriptionEn());
        }
        exposition.setSchedule(scheduleDAO.getScheduleForExposition(expoId));
        exposition.setRooms(roomDAO.getExpositionRooms(expoId));
        return exposition;
    }

    /**
     * Exposition editing Transaction.
     * Current method update exposition date in 3 steps.
     * 1. Update all fields in exposition table where expo_id = selected exposition id.
     * 2. Replace old exposition schedule values with new ones for exposition which is edited.
     * 3. Replace old records in table exposition_rooms with new one.
     *
     * @param exposition
     * Should contain description and theme in all languages, price, ExpoSchedule List should contain all date/time info,
     * Rooms List should contain at least room ids.
     * */
    public void editExposition(Exposition exposition) throws DatabaseException, SQLException {
        Connection connection = null;
        try {
            LOGGER.debug("Starting updating exposition...");
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);

            // at first, update data in exposition schedule
            expositionDAO.updateExposition(connection, exposition);

            // next, delete old schedule and set new one
            scheduleDAO.deleteScheduleForExposition(connection, exposition.getExpoId());
            for(ExpositionSchedule schedule: exposition.getSchedule()) {
                scheduleDAO.setScheduleForExposition(exposition.getExpoId(), schedule, connection);
            }

            // the last, delete old rooms and set new one
            roomDAO.removeRoomsFromExposition(connection, exposition.getExpoId());
            for (Room room: exposition.getRooms()) {
                roomDAO.setRoomsForExposition(exposition.getExpoId(), room.getRoomId(), connection);
            }

            connection.commit();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            if (connection != null) {
                connection.rollback();
            }
            throw new DatabaseException();
        } finally {
            if (connection != null) {
                connection.setAutoCommit(true);
            }
            DBManager.close(connection);
        }

        LOGGER.debug("Exposition " + exposition.getExpoId() + " has been updated successfully.");
    }

    /**
     * Current method represent exposition adding Transaction.
     * At first, new record in exposition table will be created.
     * Then, using generated key, records in Exposition_Schedule Table and Exposition_Rooms Table will be created.
     *
     * @param ticketPrice Price of 1 1ticket for exposition what will be created
     * @param textInfo Map with translated text information for exposition.
     * @param schedule List of ExpositionSchedule elements that will be set for new exposition
     * @param roomIDs Array of integers - rooms IDs which will be set for new exposition
     * */
    public void addExposition(double ticketPrice, Map<String, String> textInfo, List<ExpositionSchedule> schedule,
                              int[] roomIDs) throws SQLException, DatabaseException {
        LOGGER.debug("Starting adding exposition...");
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);

            int generatedKey = expositionDAO.addExposition(ticketPrice, textInfo, connection);

            for (ExpositionSchedule scheduleItem : schedule) {
                scheduleDAO.setScheduleForExposition(generatedKey, scheduleItem, connection);
            }

            for (int roomID : roomIDs) {
                roomDAO.setRoomsForExposition(generatedKey, roomID, connection);
            }

            connection.commit();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            LOGGER.debug("Exposition hasn't been added");
            if (connection != null) {
                connection.rollback();
            }
            throw new DatabaseException();
        } finally {
            if (connection != null) {
                connection.setAutoCommit(true);
            }
            DBManager.close(connection);
        }
        LOGGER.debug("Exposition added successfully");
    }
}
