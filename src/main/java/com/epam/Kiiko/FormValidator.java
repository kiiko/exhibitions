package com.epam.Kiiko;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormValidator {
    public static boolean isParamEmpty(HttpServletRequest req, String ... paramNames) {
        for (String paramName: paramNames) {
            if (req.getParameter(paramName) == null || "".equals(req.getParameter(paramName))) {
                return true;
            }
        }
        return false;
    }

    public static boolean isInteger(String number) {
        try {
            int _int = Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isDouble(String number) {
        try {
            double _double = Double.parseDouble(number);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isEmailValid(String email) {
        String emailRegex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);
        return matcher.find();
    }

    public static boolean isDateStringValid(String date) {
        try {
            LocalDate.parse(date);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

    public static boolean isTimeStringValid(String time) {
        try {
            LocalTime.parse(time);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

    /**
     * Method for validation date value from form
     * @param startDateString String value of start date
     * @param endDateString String value of end date
     * */
    public static boolean isDateValid(String startDateString, String endDateString) {
        if (!(isDateStringValid(startDateString) && isDateStringValid(endDateString)))
            return false;

        LocalDate startDate = LocalDate.parse(startDateString);
        LocalDate endDate = LocalDate.parse(endDateString);

        return endDate.isAfter(startDate);
    }

    /**
     * Method for validation time value from forms
     * @param startTimeString String value of start time
     * @param endTimeString String value of end time
     * */
    public static boolean isTimeValid(String startTimeString, String endTimeString) {
        if (!(isTimeStringValid(startTimeString) && isTimeStringValid(endTimeString))) {
            return false;
        }
        LocalTime startTime = LocalTime.parse(startTimeString);
        LocalTime endTime = LocalTime.parse(endTimeString);

        return endTime.isAfter(startTime);
    }
}
