package com.epam.Kiiko.entity;

public enum UserRole {
    UNREGISTERED_USER, USER, ADMIN;
}
