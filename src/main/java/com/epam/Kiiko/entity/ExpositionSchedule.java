package com.epam.Kiiko.entity;

import java.time.LocalDate;
import java.time.LocalTime;

public class ExpositionSchedule {
    private int scheduleId;
    private LocalDate startDay;
    private LocalDate endDay;
    private LocalTime startTime;
    private LocalTime endTime;

    public ExpositionSchedule() {
    }

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public LocalDate getStartDay() {
        return startDay;
    }

    public void setStartDay(LocalDate startDay) {
        this.startDay = startDay;
    }

    public LocalDate getEndDay() {
        return endDay;
    }

    public void setEndDay(LocalDate endDay) {
        this.endDay = endDay;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "ExpositionSchedule{" +
                "scheduleId=" + scheduleId +
                ", startDay=" + startDay +
                ", endDay=" + endDay +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }

    public static class Builder {
        private ExpositionSchedule schedule;

        public Builder() {
            this.schedule = new ExpositionSchedule();
        }

        public Builder withStartDate(LocalDate startDate) {
            schedule.setStartDay(startDate);
            return this;
        }

        public Builder withEndDate(LocalDate endDate) {
            schedule.setEndDay(endDate);
            return this;
        }

        public Builder withStartTime(LocalTime startTime) {
            schedule.setStartTime(startTime);
            return this;
        }

        public Builder withEndTime(LocalTime endTime) {
            schedule.setEndTime(endTime);
            return this;
        }

        public ExpositionSchedule build() {
            return schedule;
        }
    }
}
