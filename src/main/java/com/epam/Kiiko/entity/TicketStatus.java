package com.epam.Kiiko.entity;

public enum TicketStatus {
    PAID, UNPAID;
}
