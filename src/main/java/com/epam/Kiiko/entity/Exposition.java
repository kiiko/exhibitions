package com.epam.Kiiko.entity;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

public class Exposition {
    private int expoId;
    private String expoTheme;
    private String description;

    private String expoThemeEn;
    private String descriptionEn;
    private String expoThemeUa;
    private String descriptionUa;

    private double price;
    private int viewsCount;
    private List<ExpositionSchedule> schedule;
    private List<Room> rooms;

    public int getExpoId() {
        return expoId;
    }

    public void setExpoId(int expoId) {
        this.expoId = expoId;
    }

    public String getExpoTheme() {
        return expoTheme;
    }

    public void setExpoTheme(String expoTheme) {
        this.expoTheme = expoTheme;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ExpositionSchedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<ExpositionSchedule> schedule) {
        schedule.sort(Comparator.comparing(ExpositionSchedule::getStartDay));
        this.schedule = schedule;
    }

    public String getExpoThemeEn() {
        return expoThemeEn;
    }

    public void setExpoThemeEn(String expoThemeEn) {
        this.expoThemeEn = expoThemeEn;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getExpoThemeUa() {
        return expoThemeUa;
    }

    public void setExpoThemeUa(String expoThemeUa) {
        this.expoThemeUa = expoThemeUa;
    }

    public String getDescriptionUa() {
        return descriptionUa;
    }

    public void setDescriptionUa(String descriptionUa) {
        this.descriptionUa = descriptionUa;
    }

    public LocalDate getStartDate() {
        return schedule.get(0).getStartDay();
    }

    public LocalDate getLastDate() {
        return schedule.get(schedule.size() - 1).getEndDay();
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public String printRooms() {
        StringBuilder sb = new StringBuilder();
        for (Room room : rooms) {
            sb.append(room.getRoomId()).append(", ");
        }
        return sb.substring(0, sb.length() - 2).trim();
    }

    @Override
    public String toString() {
        return "Exposition{" +
                "expoId=" + expoId +
                ", expoTheme='" + expoTheme + '\'' +
                ", description='" + description + '\'' +
                ", expoThemeEn='" + expoThemeEn + '\'' +
                ", descriptionEn='" + descriptionEn + '\'' +
                ", expoThemeUa='" + expoThemeUa + '\'' +
                ", descriptionUa='" + descriptionUa + '\'' +
                ", price=" + price +
                ", viewsCount=" + viewsCount +
                ", schedule=" + schedule +
                ", rooms=" + rooms +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public static class Builder {

        private Exposition newExpo;

        public Builder() {
            this.newExpo = new Exposition();
        }

        public Builder withExpoId(int expoId) {
            newExpo.setExpoId(expoId);
            return this;
        }

        public Builder withExpoTheme(String expoTheme) {
            newExpo.setExpoTheme(expoTheme);
            return this;
        }

        public Builder withExpoThemeEn(String expoTheme) {
            newExpo.setExpoThemeEn(expoTheme);
            return this;
        }

        public Builder withExpoThemeUa(String expoTheme) {
            newExpo.setExpoThemeUa(expoTheme);
            return this;
        }

        public Builder withPrice(double price) {
            newExpo.setPrice(price);
            return this;
        }

        public Builder withViewsCount(int viewsCount) {
            newExpo.setViewsCount(viewsCount);
            return this;
        }

        public Builder withDescription(String description) {
            newExpo.setDescription(description);
            return this;
        }

        public Builder withDescriptionEn(String description) {
            newExpo.setDescriptionEn(description);
            return this;
        }

        public Builder withDescriptionUa(String description) {
            newExpo.setDescriptionUa(description);
            return this;
        }

        public Builder withSchedule(List<ExpositionSchedule> schedule) {
            newExpo.setSchedule(schedule);
            return this;
        }

        public Builder withRooms(List<Room> rooms) {
            newExpo.setRooms(rooms);
            return this;
        }

        public Exposition build() {
            return newExpo;
        }
    }
}
