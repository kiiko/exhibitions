package com.epam.Kiiko.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Ticket {
    private int ticketId;
    private int userId;
    private int expoId;
    private String expoTheme;
    private TicketStatus ticketStatus;
    private LocalDateTime creationTime;
    private double price;
    private LocalDate visitingDate;
    private int visitorsAmount;

    public Ticket() {
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getExpoId() {
        return expoId;
    }

    public void setExpoId(int expoId) {
        this.expoId = expoId;
    }

    public TicketStatus getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(TicketStatus ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDate getVisitingDate() {
        return visitingDate;
    }

    public void setVisitingDate(LocalDate visitingDate) {
        this.visitingDate = visitingDate;
    }

    public int getVisitorsAmount() {
        return visitorsAmount;
    }

    public void setVisitorsAmount(int visitors_amount) {
        this.visitorsAmount = visitors_amount;
    }

    public String getExpoTheme() {
        return expoTheme;
    }

    public void setExpoTheme(String expoTheme) {
        this.expoTheme = expoTheme;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "ticketId=" + ticketId +
                ", userId=" + userId +
                ", expoId=" + expoId +
                ", expoTheme='" + expoTheme + '\'' +
                ", ticketStatus=" + ticketStatus +
                ", creationTime=" + creationTime +
                ", price=" + price +
                ", visitingDate=" + visitingDate +
                ", visitorsAmount=" + visitorsAmount +
                '}';
    }

    public static class Builder {
        private Ticket newTicket;

        public Builder() {
            newTicket = new Ticket();
        }

        public Builder withTicketId(int ticketId) {
            newTicket.setTicketId(ticketId);
            return this;
        }

        public Builder withUserId(int userId) {
            newTicket.setUserId(userId);
            return this;
        }

        public Builder withExpoId(int expoId) {
            newTicket.setExpoId(expoId);
            return this;
        }

        public Builder withExpoTheme(String theme) {
            newTicket.setExpoTheme(theme);
            return this;
        }

        public Builder withTicketStatus(TicketStatus ticketStatus) {
            newTicket.setTicketStatus(ticketStatus);
            return this;
        }

        public Builder withCreationTime(LocalDateTime creationTime) {
            newTicket.setCreationTime(creationTime);
            return this;
        }

        public Builder withPrice(double price) {
            newTicket.setPrice(price);
            return this;
        }

        public Builder withVisitingDate(LocalDate date) {
            newTicket.setVisitingDate(date);
            return this;
        }

        public Builder withVisitorsAmount(int visitors) {
            newTicket.setVisitorsAmount(visitors);
            return this;
        }

        public Ticket build() {
            return newTicket;
        }
    }
}
