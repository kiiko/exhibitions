package com.epam.Kiiko.entity;

public class User {
    private int userId;
    private String login;
    private String password;
    private UserRole role;

    private String name;
    private String surname;
    private String email;

    public User() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", login='" + login + '\'' +
                ", role=" + role +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    /**
     * Inner class UserBuilder - is used for creating User object and to avoid large User constructor.
     * */
    public static class UserBuilder {
        private User newUser;

        public UserBuilder() {
            newUser = new User();
        }

        public UserBuilder withId(int userId) {
            newUser.setUserId(userId);
            return this;
        }

        public UserBuilder withLogin(String login) {
            newUser.setLogin(login);
            return this;
        }

        public UserBuilder withPassword(String password) {
            newUser.setPassword(password);
            return this;
        }

        public UserBuilder withRole(UserRole role) {
            newUser.setRole(role);
            return this;
        }

        public UserBuilder withName(String name) {
            newUser.setName(name);
            return this;
        }

        public UserBuilder withSurname(String surname) {
            newUser.setSurname(surname);
            return this;
        }

        public UserBuilder withEmail(String email) {
            newUser.setEmail(email);
            return this;
        }

        public User build() {
            return newUser;
        }

    }

}
