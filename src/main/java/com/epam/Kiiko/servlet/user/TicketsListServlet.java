package com.epam.Kiiko.servlet.user;

import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.dao.TicketDAO;
import com.epam.Kiiko.entity.Ticket;
import com.epam.Kiiko.entity.User;
import com.epam.Kiiko.exception.DatabaseException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.Kiiko.MessageManager.MESSAGE_DB_EXCEPTION;

@WebServlet(name = "TicketsListServlet", urlPatterns = {"/tickets-list"})
public class TicketsListServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(TicketsListServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.error("Getting users tickets.");
        User user = (User) req.getSession().getAttribute("user");
        String locale = (String) req.getSession().getAttribute("locale");

        List<Ticket> usersTickets = null;
        try {
            usersTickets = new TicketDAO().getUsersTickets(user.getUserId(), locale);
        } catch (DatabaseException e) {
            LOGGER.error(e.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        req.setAttribute("userTickets", usersTickets);
        getServletContext().getRequestDispatcher(PagePath.PAGE_USER_TICKETS_LIST).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
