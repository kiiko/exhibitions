package com.epam.Kiiko.servlet.user;

import com.epam.Kiiko.FormValidator;
import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.dao.TicketDAO;
import com.epam.Kiiko.entity.User;
import com.epam.Kiiko.exception.DatabaseException;
import com.epam.Kiiko.service.TicketService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.Kiiko.MessageManager.*;

@WebServlet(name = "DeleteTicketServlet", urlPatterns = {"/delete-ticket"})
public class DeleteTicketServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(DeleteTicketServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("Started deleting ticket");
        if (FormValidator.isParamEmpty(req, "ticketId")) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_EMPTY_PARAMS));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        int ticketId = Integer.parseInt(req.getParameter("ticketId"));
        User user = (User) req.getSession().getAttribute("user");
        TicketDAO ticketDAO = new TicketDAO();

        try {
            if (new TicketService().isInvalidTicketStatusForUser(ticketId, user.getUserId())) {
                req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
                getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
                return;
            }

            ticketDAO.deleteTicket(ticketId);

        } catch (DatabaseException e) {
            LOGGER.error(e.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        LOGGER.debug("Ticket " + ticketId + " has been deleted.");
        resp.sendRedirect("tickets-list");
    }
}
