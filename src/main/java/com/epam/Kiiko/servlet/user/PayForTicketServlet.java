package com.epam.Kiiko.servlet.user;

import com.epam.Kiiko.FormValidator;
import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.dao.TicketDAO;
import com.epam.Kiiko.entity.Ticket;
import com.epam.Kiiko.entity.User;
import com.epam.Kiiko.exception.DatabaseException;
import com.epam.Kiiko.service.TicketService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.Kiiko.MessageManager.MESSAGE_DB_EXCEPTION;

@WebServlet(name = "PayForTicketServlet", urlPatterns = {"/pay-for-ticket"})
public class PayForTicketServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(PayForTicketServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("Started PayForTicket#doGet()");
        if (FormValidator.isParamEmpty(req, "ticketId")) {
            LOGGER.debug("Invalid parameter " + req.getParameter("ticketId"));
            req.setAttribute("errorMessage", "Invalid parameter");
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        int ticketId = Integer.parseInt(req.getParameter("ticketId"));
        User user = (User) req.getSession().getAttribute("user");
        String locale = (String) req.getSession().getAttribute("locale");
        Ticket ticket = null;

        try {
            if (new TicketService().isInvalidTicketStatusForUser(ticketId, user.getUserId())) {
                req.setAttribute("errorMessage", "Wrong parameters");
                getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
                return;
            }

            ticket = new TicketDAO().getTicket(ticketId, locale);
        } catch (DatabaseException e) {
            LOGGER.error(e.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }
        req.setAttribute("ticket", ticket);
        getServletContext().getRequestDispatcher(PagePath.PAGE_USER_PAY_FOR_TICKET).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("Started PayForTicket#doPost()");
        if (FormValidator.isParamEmpty(req, "ticketId", "cardNumber", "cvv", "expirationDate")) {
            LOGGER.debug("Empty params in PayForTicket#doPost()");
            req.setAttribute("errorMessage", "Invalid parameter");
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        int ticketId = Integer.parseInt(req.getParameter("ticketId"));
        User user = (User) req.getSession().getAttribute("user");

        try {
            if (new TicketService().isInvalidTicketStatusForUser(ticketId, user.getUserId())) {
                req.setAttribute("errorMessage", "Wrong parameters");
                getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
                return;
            }

            new TicketDAO().changeTicketStatus(ticketId);
        } catch (DatabaseException e) {
            LOGGER.error(e.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }
        LOGGER.debug("Ticket " + ticketId + " is payed successfully");
        resp.sendRedirect("message?event=ticketPaid");
    }
}
