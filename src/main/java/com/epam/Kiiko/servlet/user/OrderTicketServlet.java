package com.epam.Kiiko.servlet.user;

import com.epam.Kiiko.FormValidator;
import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.dao.ExpositionDAO;
import com.epam.Kiiko.dao.TicketDAO;
import com.epam.Kiiko.entity.Exposition;
import com.epam.Kiiko.entity.User;
import com.epam.Kiiko.exception.DatabaseException;
import com.epam.Kiiko.service.ExpositionService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;

import static com.epam.Kiiko.MessageManager.*;

@WebServlet(name = "OrderTicketServlet", urlPatterns = {"/order-ticket"})
public class OrderTicketServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(OrderTicketServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("Get exposition info for ordering ticket");
        if (FormValidator.isParamEmpty(req, "expoId")
                || !FormValidator.isInteger(req.getParameter("expoId"))
                || Integer.parseInt(req.getParameter("expoId")) < 1) {
            LOGGER.debug("Invalid parameter " + req.getParameter("expoId"));
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }
        int expoId = Integer.parseInt(req.getParameter("expoId"));
        try {
            if (!new ExpositionDAO().ifExpositionExist(expoId)) {
                LOGGER.debug("Invalid parameter " + req.getParameter("expoId"));
                req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
                getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
                return;
            }
        } catch (DatabaseException e) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }
        String locale = (String) req.getSession().getAttribute("locale");
        Exposition exposition = new ExpositionService().getExpositionDetails(expoId, locale);

        req.setAttribute("exposition", exposition);
        LOGGER.debug("Gave for ticket ordering information about exposition " + exposition);
        req.getRequestDispatcher(PagePath.PAGE_USER_ORDER_TICKET).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("Starting ticket ordering");
        if (FormValidator.isParamEmpty(req, "visitorsAmount", "visitingDate", "expoId")
                || !FormValidator.isDateStringValid(req.getParameter("visitingDate"))
                || !FormValidator.isInteger(req.getParameter("visitorsAmount"))
                || !FormValidator.isInteger(req.getParameter("expoId"))) {
            LOGGER.debug("Invalid parameters");
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
            getServletContext().getRequestDispatcher(PagePath.PAGE_USER_ORDER_TICKET).forward(req, resp);
            return;
        }

        int visitorsAmount = Integer.parseInt(req.getParameter("visitorsAmount"));
        LocalDate visitingDate = LocalDate.parse(req.getParameter("visitingDate"));

        if (visitorsAmount < 1 || visitingDate.isBefore(LocalDate.now())) {
            LOGGER.debug("Invalid parameter " + visitorsAmount + " or " + visitingDate + " is before now");
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
            doGet(req, resp);
            getServletContext().getRequestDispatcher(PagePath.PAGE_USER_ORDER_TICKET).forward(req, resp);
            return;
        }

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        int expositionId = Integer.parseInt(req.getParameter("expoId"));

        int newTicketId;
        try {
            if (!new ExpositionDAO().ifExpositionExist(expositionId)) {
                LOGGER.debug("Invalid parameter " + req.getParameter("expoId"));
                req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
                getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
                return;
            }
            newTicketId = new TicketDAO().createTicket(user.getUserId(), expositionId, visitorsAmount, visitingDate);
        } catch (DatabaseException e) {
            LOGGER.error(e.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        LOGGER.debug("Ticket with id" + newTicketId + " is successfully ordered");
        resp.sendRedirect("message?event=ticketOrdered&ticketId=" + newTicketId);
    }
}
