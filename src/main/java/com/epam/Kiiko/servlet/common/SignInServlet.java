package com.epam.Kiiko.servlet.common;

import com.epam.Kiiko.FormValidator;
import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.dao.UserDAO;
import com.epam.Kiiko.entity.User;
import com.epam.Kiiko.exception.DatabaseException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.Kiiko.MessageManager.*;

@WebServlet(name = "SignInServlet", urlPatterns = {"/sign-in"})
public class SignInServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(SignInServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("SignInServlet#doGet start");
        req.getRequestDispatcher(PagePath.PAGE_SIGN_IN).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("SignInServlet#doPost start");

        if (FormValidator.isParamEmpty(req, "login", "password")) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_EMPTY_PARAMS));
            req.getRequestDispatcher(PagePath.PAGE_SIGN_IN).forward(req, resp);
            return;
        }

        String login = req.getParameter("login");
        String password = req.getParameter("password");

        User user = null;
        try {
            user = new UserDAO().getUserByLoginOrEmail(login);
        } catch (DatabaseException e) {
            LOGGER.error(e.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            req.getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        if (user == null || !user.getPassword().equals(password)) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_WRONG_SIGN_IN));
            req.getRequestDispatcher(PagePath.PAGE_SIGN_IN).forward(req, resp);
            return;
        }

        HttpSession session = req.getSession();
        session.setAttribute("user", user);
        resp.sendRedirect("expositions");
    }
}
