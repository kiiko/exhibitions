package com.epam.Kiiko.servlet.common;

import com.epam.Kiiko.FormValidator;
import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.dao.UserDAO;
import com.epam.Kiiko.entity.User;
import com.epam.Kiiko.entity.UserRole;
import com.epam.Kiiko.exception.DatabaseException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.Kiiko.MessageManager.*;

@WebServlet(name = "UserRegistrationServlet", urlPatterns = {"/registration"})
public class RegistrationServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(RegistrationServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(PagePath.PAGE_REGISTRATION).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (FormValidator.isParamEmpty(req, "login", "password", "username", "surname", "email")) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_EMPTY_PARAMS));
            getServletContext().getRequestDispatcher(PagePath.PAGE_REGISTRATION).forward(req, resp);
            return;
        }

        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String username = req.getParameter("username");
        String surname = req.getParameter("surname");
        String email = req.getParameter("email");

        if (!FormValidator.isEmailValid(email)) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_EMAIL));
            getServletContext().getRequestDispatcher(PagePath.PAGE_REGISTRATION).forward(req, resp);
            return;
        }

        UserRole role = UserRole.USER;

        if (req.getSession().getAttribute("user") != null
                && ((User)req.getSession().getAttribute("user")).getRole() == UserRole.ADMIN ) {
            role = UserRole.valueOf(req.getParameter("userRole").toUpperCase());
        }

        try {
            if (!new UserDAO().areAvailableEmailAndLogin(email, login)) {
                req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_LOGIN_NOT_AVAILABLE));
                getServletContext().getRequestDispatcher(PagePath.PAGE_REGISTRATION).forward(req, resp);
                return;
            }
            User newUser = new User.UserBuilder()
                    .withName(username)
                    .withLogin(login)
                    .withEmail(email)
                    .withPassword(password)
                    .withSurname(surname)
                    .withRole(role)
                    .build();

            new UserDAO().addUser(newUser);
        } catch (DatabaseException e) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }
        if (req.getSession().getAttribute("user") != null) {
            resp.sendRedirect("expositions");
            return;
        }
        resp.sendRedirect("sign-in");
    }
}
