package com.epam.Kiiko.servlet.common;

import com.epam.Kiiko.PagePath;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "MessageServlet", urlPatterns = {"/message"})
public class MessageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("event", req.getParameter("event"));
        if (req.getParameter("event").equals("ticketOrdered")) {
            req.setAttribute("ticketId", req.getParameter("ticketId"));
        }
        getServletContext().getRequestDispatcher(PagePath.PAGE_MESSAGE).forward(req, resp);
    }
}
