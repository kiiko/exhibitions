package com.epam.Kiiko.servlet.common;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "SignOutServlet", urlPatterns = {"/sign-out"})
public class SignOutServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(SignOutServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOGGER.debug("SignOutServlet starts.");
        HttpSession session = req.getSession();
        session.invalidate();
        resp.sendRedirect("expositions");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
