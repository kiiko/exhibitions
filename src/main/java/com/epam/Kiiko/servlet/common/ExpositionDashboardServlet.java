package com.epam.Kiiko.servlet.common;

import com.epam.Kiiko.FormValidator;
import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.entity.Exposition;
import com.epam.Kiiko.entity.User;
import com.epam.Kiiko.entity.UserRole;
import com.epam.Kiiko.exception.DatabaseException;
import com.epam.Kiiko.exception.InvalidParameterException;
import com.epam.Kiiko.service.ExpoSearchDetails;
import com.epam.Kiiko.service.ExpositionService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static com.epam.Kiiko.MessageManager.*;

@WebServlet(name = "ExpositionDashboardServlet",
        urlPatterns = {"/expositions"})
public class ExpositionDashboardServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(ExpositionDashboardServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("Started getting info for exposition dashboard");
        ExpoSearchDetails searchDetails = null;
        try {
            searchDetails = getSearchDetails(req);
        } catch (InvalidParameterException e) {
            LOGGER.error(e.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }
        LOGGER.debug(searchDetails);
        int expositionCount = 0;
        try {
            expositionCount = new ExpositionService().getExpositionCount(searchDetails);
        } catch (DatabaseException e) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        if (expositionCount == 0) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_NO_EXPO_FOUND));
            getServletContext().getRequestDispatcher(PagePath.PAGE_EXPOSITION_LIST).forward(req, resp);
            return;
        }

        int currentPage = 1;
        int recordsPerPage = 5;

        int pagesCount = expositionCount / recordsPerPage;

        if (!FormValidator.isParamEmpty(req,"currentPage")
                && FormValidator.isInteger(req.getParameter("currentPage"))) {
            currentPage = Integer.parseInt(req.getParameter("currentPage"));
            if (currentPage > pagesCount) {
                currentPage = 1;
            }
        }
        int firstRow = (currentPage - 1) * recordsPerPage;
        searchDetails.setRows(firstRow, recordsPerPage);

        List<Exposition> expositionList = null;

        try {
            expositionList = new ExpositionService().getExpositionList(searchDetails);
        } catch (DatabaseException ex) {
            LOGGER.error(ex.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        } catch (IllegalArgumentException ex) {
            LOGGER.error(ex.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
            getServletContext().getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        req.setAttribute("prevSearchDetails", searchDetails);
        req.setAttribute("expositionList", expositionList);
        req.setAttribute("pagesCount", pagesCount);
        req.setAttribute("currentPage", currentPage);
        req.setAttribute("currentGetRequestParams", generateReqParams(searchDetails));

        req.getRequestDispatcher(PagePath.PAGE_EXPOSITION_LIST).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    /**
     * This method received details for exposition searching from request.
     */
    public ExpoSearchDetails getSearchDetails(HttpServletRequest req) throws InvalidParameterException {
        HttpSession session = req.getSession();
        String locale = "en";
        UserRole userRole = UserRole.UNREGISTERED_USER;

        if (session.getAttribute("user") != null) {
            userRole = ((User) session.getAttribute("user")).getRole();
        }
        if (session.getAttribute("locale") != null) {
            locale = (String) session.getAttribute("locale");
        }

        ExpoSearchDetails searchDetails;

        if (FormValidator.isParamEmpty(req, "searchBy")) {
            return new ExpoSearchDetails(locale, ExpoSearchDetails.SEARCH_BY_DEFAULT, userRole);
        }

        String searchBy = req.getParameter("searchBy");

        if (searchBy.equals(ExpoSearchDetails.SEARCH_BY_THEME)) {
            String theme = req.getParameter("themeValue");
            if (FormValidator.isParamEmpty(req, "themeValue")) {
                throw new InvalidParameterException();
            }
            searchDetails = new ExpoSearchDetails(locale, ExpoSearchDetails.SEARCH_BY_THEME, userRole);
            searchDetails.setForThemeSearch(theme);
            return searchDetails;
        }

        if (searchBy.equals(ExpoSearchDetails.SEARCH_BY_PRICE)) {
            if (FormValidator.isParamEmpty(req, "priceFrom", "priceTo")
                    || !FormValidator.isDouble(req.getParameter("priceFrom"))
                    || !FormValidator.isDouble(req.getParameter("priceTo"))) {
                throw new InvalidParameterException();
            }
            double priceFrom = Double.parseDouble(req.getParameter("priceFrom"));
            double priceTo = Double.parseDouble(req.getParameter("priceTo"));
            if (priceFrom > priceTo) {
                throw new InvalidParameterException();
            }
            searchDetails = new ExpoSearchDetails(locale, ExpoSearchDetails.SEARCH_BY_PRICE, userRole);
            searchDetails.setForPriceSearch(priceFrom, priceTo);
            return searchDetails;
        }

        if (searchBy.equals(ExpoSearchDetails.SEARCH_BY_DATE)) {
            if (FormValidator.isParamEmpty(req, "dateFrom", "dateTo") ||
                    !FormValidator.isDateValid(req.getParameter("dateFrom"), req.getParameter("dateTo"))) {
                throw new InvalidParameterException();
            }
            LocalDate dateFrom = LocalDate.parse(req.getParameter("dateFrom"));
            LocalDate dateTo = LocalDate.parse(req.getParameter("dateTo"));
            searchDetails = new ExpoSearchDetails(locale, ExpoSearchDetails.SEARCH_BY_DATE, userRole);
            searchDetails.setForDateSearch(dateFrom, dateTo);
            return searchDetails;
        }
        searchDetails = new ExpoSearchDetails(locale, ExpoSearchDetails.SEARCH_BY_DEFAULT, userRole);
        return searchDetails;
    }

    /**
     * Current method creates string with GET request parameters. It is using for pagination on exhibition-display.jsp
     */
    public String generateReqParams(ExpoSearchDetails searchDetails) {
        String params = "&searchBy=" + searchDetails.getSearchBy();
        if (searchDetails.getSearchBy().equals(ExpoSearchDetails.SEARCH_BY_THEME)) {
            params += "&themeValue=" + searchDetails.getTheme();
        }
        if (searchDetails.getSearchBy().equals(ExpoSearchDetails.SEARCH_BY_DATE)) {
            params += "&dateFrom=" + searchDetails.getDateFrom() + "&dateTo=" + searchDetails.getDateTo();
        }
        if (searchDetails.getSearchBy().equals(ExpoSearchDetails.SEARCH_BY_PRICE)) {
            params += "&priceFrom=" + searchDetails.getPriceFrom() + "&priceTo=" + searchDetails.getPriceTo();
        }
        return params;
    }
}
