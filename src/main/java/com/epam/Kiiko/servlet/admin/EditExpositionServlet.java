package com.epam.Kiiko.servlet.admin;

import com.epam.Kiiko.FormValidator;
import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.dao.ExpositionDAO;
import com.epam.Kiiko.dao.RoomDAO;
import com.epam.Kiiko.entity.Exposition;
import com.epam.Kiiko.entity.ExpositionSchedule;
import com.epam.Kiiko.entity.Room;
import com.epam.Kiiko.exception.DatabaseException;
import com.epam.Kiiko.exception.InvalidParameterException;
import com.epam.Kiiko.service.ExpositionService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static com.epam.Kiiko.MessageManager.*;

@WebServlet(name = "EditExpositionServlet", urlPatterns = {"/edit-exposition"})
public class EditExpositionServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(EditExpositionServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("EditExpositionServlet#doGet has been started");
        if (req.getSession().getAttribute("updatedSchedule") != null) {
            req.getSession().removeAttribute("updatedSchedule");
        }

        if (req.getSession().getAttribute("expositionToBeUpdated") != null) {
            req.getSession().removeAttribute("expositionToBeUpdated");
        }

        if (FormValidator.isParamEmpty(req, "expoId") || !FormValidator.isInteger(req.getParameter("expoId"))) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
            req.getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        int expoId = Integer.parseInt(req.getParameter("expoId"));
        Exposition exposition = null;
        try {
            if (! new ExpositionDAO().ifExpositionExist(expoId)) {
                req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
                req.getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
                return;
            }
            exposition = new ExpositionService().getExpositionDetails(expoId, String.valueOf(req.getSession().getAttribute("locale")));
        } catch (DatabaseException e) {
            LOGGER.error(e.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            req.getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }
        req.getSession().setAttribute("expositionToBeUpdated", exposition);
        req.getRequestDispatcher(PagePath.PAGE_ADMIN_EDIT_EXPOSITION).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            LOGGER.debug("Editing step " + req.getParameter("editingStep"));
            if ("1".equals(req.getParameter("editingStep"))) {
                LOGGER.debug("Updating exposition step 1 started");

                firstEditingStep(req);

                LOGGER.debug("Updating exposition step 1 finished");
                req.getRequestDispatcher(PagePath.PAGE_ADMIN_EDIT_EXPOSITION).forward(req, resp);
            } else if ("2".equals(req.getParameter("editingStep"))) {
                LOGGER.debug("Updating exposition step 2 started");

                secondEditingStep(req);

                req.getSession().removeAttribute("updatedSchedule");
                req.getSession().removeAttribute("expositionToBeUpdated");
                LOGGER.debug("Updating exposition step 2 finished");
                resp.sendRedirect("expositions");
            }
        } catch (InvalidParameterException e) {
            req.getRequestDispatcher(PagePath.PAGE_ADMIN_EDIT_EXPOSITION).forward(req, resp);
        } catch (DatabaseException | SQLException e) {
            req.getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
        }
    }

    /**
     * Current method does first step in editing - getting available rooms for new time periods
     */
    public void firstEditingStep(HttpServletRequest req) throws InvalidParameterException, DatabaseException {
        int periods = Integer.parseInt(req.getParameter("periodsNumber"));
        List<ExpositionSchedule> schedule = new ArrayList<>();
        for (int i = 1; i <= periods; i++) {
            if (FormValidator.isParamEmpty(req, "dateFrom" + i, "dateTo" + i, "timeFrom" + i, "timeTo" + 1)
                    || !FormValidator.isDateValid(req.getParameter("dateFrom" + i), req.getParameter("dateTo" + i))
                    || !FormValidator.isTimeValid(req.getParameter("timeFrom" + i), req.getParameter("timeTo" + i))) {
                LOGGER.debug("Invalid parameters in firstEditingStep");
                req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_DATE));
                throw new InvalidParameterException();
            }

            schedule.add(new ExpositionSchedule.Builder()
                    .withStartDate(LocalDate.parse(req.getParameter("dateFrom" + i)))
                    .withEndDate(LocalDate.parse(req.getParameter("dateTo" + i)))
                    .withStartTime(LocalTime.parse(req.getParameter("timeFrom" + i)))
                    .withEndTime(LocalTime.parse(req.getParameter("timeTo" + 1)))
                    .build());
        }
        LOGGER.debug("Schedule extracting finished");
        schedule.sort(Comparator.comparing(ExpositionSchedule::getStartDay));

        LocalDate startDate = schedule.get(0).getStartDay();
        LocalDate endDate = schedule.get(schedule.size() - 1).getEndDay();

        int expoId = ((Exposition) req.getSession().getAttribute("expositionToBeUpdated")).getExpoId();

        List<Room> rooms = new RoomDAO().getAvailableRoomsForEditing(startDate, endDate, expoId);
        req.setAttribute("editingStep", 2);
        req.setAttribute("availableRooms", rooms);
        req.getSession().setAttribute("updatedSchedule", schedule);
    }

    /**
     * Current method updates exposition
     */
    public void secondEditingStep(HttpServletRequest req) throws InvalidParameterException, DatabaseException, SQLException {
        LOGGER.debug("secondEditingStep started");
        if (FormValidator.isParamEmpty(req, "themeEN", "themeUA", "descriptionEN", "descriptionUA", "price", "rooms")
                || !FormValidator.isDouble(req.getParameter("price"))) {
            LOGGER.debug("Invalid parameters in processSecondStep()");
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
            throw new InvalidParameterException();
        }
        double ticketPrice = Double.parseDouble(req.getParameter("price"));
        List<ExpositionSchedule> updatedSchedule = (List<ExpositionSchedule>) req.getSession().getAttribute("updatedSchedule");

        String[] roomIdValues = req.getParameterValues("rooms");
        List<Room> updatedRooms = new ArrayList<>();
        for (String roomIdValue : roomIdValues) {
            Room room = new Room();
            room.setRoomId(Integer.parseInt(roomIdValue));
            updatedRooms.add(room);
        }

        Exposition expoToBeUpdated = (Exposition) req.getSession().getAttribute("expositionToBeUpdated");
        expoToBeUpdated.setDescriptionEn(req.getParameter("descriptionEN"));
        expoToBeUpdated.setDescriptionUa(req.getParameter("descriptionUA"));
        expoToBeUpdated.setExpoThemeUa(req.getParameter("themeUA"));
        expoToBeUpdated.setExpoThemeEn(req.getParameter("themeEN"));
        expoToBeUpdated.setSchedule(updatedSchedule);
        expoToBeUpdated.setPrice(ticketPrice);
        expoToBeUpdated.setRooms(updatedRooms);

        new ExpositionService().editExposition(expoToBeUpdated);
        LOGGER.debug("secondEditingStep finished");
    }
}
