package com.epam.Kiiko.servlet.admin;

import com.epam.Kiiko.FormValidator;
import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.dao.RoomDAO;
import com.epam.Kiiko.entity.ExpositionSchedule;
import com.epam.Kiiko.entity.Room;
import com.epam.Kiiko.exception.DatabaseException;
import com.epam.Kiiko.exception.InvalidParameterException;
import com.epam.Kiiko.service.ExpositionService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static com.epam.Kiiko.MessageManager.*;

@WebServlet(name = "AddExpositionServlet", urlPatterns = "/add-exposition")
public class AddExpositionServlet extends HttpServlet {
    public static final Logger LOGGER = Logger.getLogger(AddExpositionServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("selectedSchedule") != null) {
            req.getSession().removeAttribute("selectedSchedule");
        }
        req.getRequestDispatcher(PagePath.PAGE_ADMIN_ADD_EXPOSITION).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("Exposition adding start");
        int step = Integer.parseInt(req.getParameter("step"));
        LOGGER.debug("Current step is " + step);
        if (step == 1) {
            try {
                processFirstStep(req);
            } catch (InvalidParameterException e) {
                LOGGER.error(e.getMessage());
                req.getRequestDispatcher(PagePath.PAGE_ADMIN_ADD_EXPOSITION).forward(req, resp);
                return;
            }
            LOGGER.debug("Step 1 is finished" + step);
            req.getRequestDispatcher(PagePath.PAGE_ADMIN_ADD_EXPOSITION).forward(req, resp);
        } else if (step == 2) {
            try {

                processSecondStep(req);

            } catch (DatabaseException | SQLException e) {
                LOGGER.error(e.getMessage());
                req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
                req.getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
                return;
            } catch (InvalidParameterException e) {
                req.setAttribute("step", 2);
                req.getRequestDispatcher(PagePath.PAGE_ADMIN_ADD_EXPOSITION).forward(req, resp);
                return;
            }
            LOGGER.debug("Step 2 is finished" + step);
            resp.sendRedirect("expositions");
        }

    }

    /**
     * On first step, reading all date info from user and find available rooms for selected date period
     */
    private void processFirstStep(HttpServletRequest req) throws InvalidParameterException {
        LOGGER.debug("processFirstStep() started");
        int periods = Integer.parseInt(req.getParameter("periodsNumber"));
        List<ExpositionSchedule> schedule = new ArrayList<>();
        for (int i = 1; i <= periods; i++) {
            ///////
            // Check if date/time parameters are empty or invalid (invalid string or start > end)
            ///////
            if (FormValidator.isParamEmpty(req, "dateFrom" + i, "dateTo" + i, "timeFrom" + i, "timeTo" + 1)
                    || !FormValidator.isDateValid(req.getParameter("dateFrom" + i), req.getParameter("dateTo" + i))
                    || !FormValidator.isTimeValid(req.getParameter("timeFrom" + i), req.getParameter("timeTo" + i))) {
                LOGGER.debug("Invalid parameters in processFirstStep()");
                req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_DATE));
                throw new InvalidParameterException();
            }
            if (LocalDate.parse(req.getParameter("dateFrom" + i)).isBefore(LocalDate.now())) {
                LOGGER.debug("Invalid parameters in processFirstStep()");
                req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_START_DATE));
                throw new InvalidParameterException();
            }
            schedule.add(new ExpositionSchedule.Builder()
                    .withStartDate(LocalDate.parse(req.getParameter("dateFrom" + i)))
                    .withEndDate(LocalDate.parse(req.getParameter("dateTo" + i)))
                    .withStartTime(LocalTime.parse(req.getParameter("timeFrom" + i)))
                    .withEndTime(LocalTime.parse(req.getParameter("timeTo" + 1)))
                    .build());
        }
        schedule.sort(Comparator.comparing(ExpositionSchedule::getStartDay));
        LocalDate startDate = schedule.get(0).getStartDay();
        LocalDate endDate = schedule.get(schedule.size() - 1).getEndDay();

        List<Room> rooms = new RoomDAO().getAvailableRooms(startDate, endDate);
        req.setAttribute("step", 2);
        req.setAttribute("availableRooms", rooms);
        req.getSession().setAttribute("selectedSchedule", schedule);
        LOGGER.debug("processFirstStep() finished");
    }

    /**
     * On second step, adding reading all info and adding into DB
     */
    private void processSecondStep(HttpServletRequest req) throws DatabaseException, SQLException, InvalidParameterException {
        LOGGER.debug("processSecondStep() started");
        if (FormValidator.isParamEmpty(req, "themeEN", "themeUA", "descriptionEN", "descriptionUA", "price", "rooms")
                || !FormValidator.isDouble(req.getParameter("price"))) {
            LOGGER.debug("Invalid parameters in processSecondStep()");
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
            throw new InvalidParameterException();
        }
        Map<String, String> textInfo = new HashMap<>();
        textInfo.put("themeEN", req.getParameter("themeEN"));
        textInfo.put("themeUA", req.getParameter("themeUA"));
        textInfo.put("descriptionEN", req.getParameter("descriptionEN"));
        textInfo.put("descriptionUA", req.getParameter("descriptionUA"));

        double ticketPrice = Double.parseDouble(req.getParameter("price"));

        List<ExpositionSchedule> schedules = (List<ExpositionSchedule>) req.getSession().getAttribute("selectedSchedule");

        String[] roomIdValues = req.getParameterValues("rooms");
        int[] roomIDs = new int[roomIdValues.length];
        for (int i = 0; i < roomIdValues.length; i++) {
            roomIDs[i] = Integer.parseInt(roomIdValues[i]);
        }

        new ExpositionService().addExposition(ticketPrice, textInfo, schedules, roomIDs);
        LOGGER.debug("processSecondStep() finished");
    }
}
