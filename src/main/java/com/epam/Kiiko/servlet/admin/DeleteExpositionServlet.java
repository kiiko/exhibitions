package com.epam.Kiiko.servlet.admin;

import com.epam.Kiiko.FormValidator;
import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.dao.ExpositionDAO;
import com.epam.Kiiko.exception.DatabaseException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.Kiiko.MessageManager.MESSAGE_DB_EXCEPTION;
import static com.epam.Kiiko.MessageManager.MESSAGE_INVALID_REQUEST_PARAMS;

@WebServlet(name = "DeleteExpositionServlet", urlPatterns = {"/delete-exposition"})
public class DeleteExpositionServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(DeleteExpositionServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("exposition deleting start");
        if (FormValidator.isParamEmpty(req, "expoId")) {
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
            req.getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
            return;
        }

        int expoId = Integer.parseInt(req.getParameter("expoId"));
        try {

            if (! new ExpositionDAO().ifExpositionExist(expoId)) {
                req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_INVALID_REQUEST_PARAMS));
                req.getRequestDispatcher(PagePath.PAGE_ERROR).forward(req, resp);
                return;
            }

            new ExpositionDAO().deleteExposition(expoId);
        } catch (DatabaseException e) {
            LOGGER.error(e.getMessage());
            req.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_DB_EXCEPTION));
            req.getRequestDispatcher(PagePath.PAGE_USER_ORDER_TICKET).forward(req, resp);
            return;
        }
        LOGGER.debug("Exposition deleting end");
        resp.sendRedirect("expositions");
    }
}
