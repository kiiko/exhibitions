package com.epam.Kiiko.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import java.io.IOException;

public class EncodingFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(EncodingFilter.class);

    private String encoding;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.debug("Encoding filter initialization...");
        encoding = filterConfig.getInitParameter("encoding");
        if (encoding == null) {
            encoding = "UTF-8";
        }
        LOGGER.debug(encoding + " was set");
        LOGGER.debug("EncodingFilter is initialized.");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOGGER.debug("Encoding filter starts...");
        LOGGER.debug("Request encoding before filter: " + request.getCharacterEncoding());
        if (request.getCharacterEncoding() == null) {
            request.setCharacterEncoding(encoding);
        }

        response.setCharacterEncoding(encoding);
        LOGGER.debug("Request encoding after filter: " + request.getCharacterEncoding());
        LOGGER.debug("Encoding filter ends...");
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        LOGGER.debug("Encoding filter destruction...");
    }
}
