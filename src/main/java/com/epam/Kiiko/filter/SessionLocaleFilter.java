package com.epam.Kiiko.filter;

import com.epam.Kiiko.MessageManager;
import com.epam.Kiiko.entity.UserRole;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class SessionLocaleFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(SessionLocaleFilter.class);
    private static List<String> languages = new ArrayList<>();
    private static String defaultLanguage;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.debug("Session locale filter initialization started");

        languages = asList(filterConfig.getInitParameter("languages"));
        LOGGER.debug("Languages: " + languages);

        defaultLanguage = filterConfig.getInitParameter("default-language");
        LOGGER.debug("Default language: " + defaultLanguage);

        LOGGER.debug("Session locale filter initialization ended");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOGGER.debug("Start SessionLocaleFilter");
        HttpServletRequest req = (HttpServletRequest) request;

        if (req.getSession().getAttribute("locale") == null) {
            LOGGER.debug("Session locale is empty -> setting 'en' as session locale");
            req.getSession().setAttribute("locale", defaultLanguage);
        }

        if (req.getParameter("language") != null) {
            String newLanguage = req.getParameter("language");
            if (languages.contains(newLanguage)) {
                req.getSession().setAttribute("locale", newLanguage);
                MessageManager.changeLocale(newLanguage);
                LOGGER.debug("Session locale has been changed to " + newLanguage);
            } else {
                LOGGER.debug("Session locale can't be changed to " + newLanguage + " -> setting "
                        + defaultLanguage + " as session locale");
                req.getSession().setAttribute("locale", defaultLanguage);
            }

        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        LOGGER.debug("Session locale filter destroyed");
    }

    /**
     * Current method return list of languages Strings extracted from languages String.
     */
    private List<String> asList(String str) {
        List<String> list = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(str);
        while (st.hasMoreTokens()) {
            list.add(st.nextToken());
        }
        return list;
    }
}
