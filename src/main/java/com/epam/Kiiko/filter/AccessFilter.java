package com.epam.Kiiko.filter;

import com.epam.Kiiko.MessageManager;
import static com.epam.Kiiko.MessageManager.MESSAGE_ACCESS_NOT_ALLOWED;
import com.epam.Kiiko.PagePath;
import com.epam.Kiiko.entity.User;
import com.epam.Kiiko.entity.UserRole;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

public class AccessFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(AccessFilter.class);
    private static final Map<UserRole, List<String>> roleAccess = new HashMap<>();
    private static List<String> commonAccess = new ArrayList<>();
    private static List<String> noFilter = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) {
        LOGGER.debug("Access filter initialization started");

        roleAccess.put(UserRole.ADMIN, asList(filterConfig.getInitParameter("admin")));
        roleAccess.put(UserRole.USER, asList(filterConfig.getInitParameter("user")));
        LOGGER.debug("Filter roleAccess: " + roleAccess);
        commonAccess = asList(filterConfig.getInitParameter("common"));
        LOGGER.debug("Filter commonAccess: " + commonAccess);
        noFilter = asList(filterConfig.getInitParameter("outOfControl"));
        LOGGER.debug("Filter outOfControl: " + noFilter);

        LOGGER.debug("Access filter initialization ended");
    }

    @Override
    public void destroy() {
        LOGGER.debug("Filter destruction");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOGGER.debug("Filter started");
        if (allowAccess(request)) {
            LOGGER.debug("AccessFilter finished - access allowed");
            chain.doFilter(request, response);
        } else {
            LOGGER.debug("AccessFilter finished - access denied");
            request.setAttribute("errorMessage", MessageManager.getMessage(MESSAGE_ACCESS_NOT_ALLOWED));
            request.getRequestDispatcher(PagePath.PAGE_ERROR).forward(request, response);
        }

    }

    private boolean allowAccess(ServletRequest request) {
        HttpServletRequest req = (HttpServletRequest) request;
        String reqURI = req.getRequestURI();
        LOGGER.debug("Request URI " + reqURI);
        if (noFilter.contains(reqURI) || commonAccess.contains(reqURI)) {
            return true;
        }
        User user = (User) req.getSession().getAttribute("user");
        if (user == null) {
            return false;
        }
        UserRole userRole = user.getRole();
        LOGGER.debug("User ROLE " + userRole);
        return roleAccess.get(userRole).contains(reqURI);
    }

    /**
     * Current method return list of requestURI Strings extracted from initParameter String.
     */
    private List<String> asList(String str) {
        String commonPart = "/Exhibitions";
        List<String> list = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(str);
        while (st.hasMoreTokens()) {
            list.add(commonPart + st.nextToken());
        }
        return list;
    }
}
