<%@ include file="/WEB-INF/jspf/directives/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directives/taglib.jspf" %>
<%@ page isErrorPage="true" %>
<html>
<head>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>
<body>
<div id="centerDiv">
    <%@ include file="/WEB-INF/jspf/header.jspf" %>
    <div class="section">
        <c:if test="${pageScope.errorData.statusCode} eq '404'">
            <h2>404</h2>
            <h3><fmt:message key="page.error.not_found"/> </h3>
        </c:if>
        <h3><c:out value="${requestScope.errorMessage}"/></h3>
    </div>
</div>
</body>
</html>
