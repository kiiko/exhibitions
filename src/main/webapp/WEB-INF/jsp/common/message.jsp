<%--
  Created by IntelliJ IDEA.
  User: Kiiko
  Date: 18.02.2021
  Time: 13:29
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jspf/directives/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directives/taglib.jspf" %>
<html>
<head>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>
<body>
<div id="centerDiv">
    <%@ include file="/WEB-INF/jspf/header.jspf" %>
    <div class="section">
        <c:choose>
            <c:when test="${requestScope.event eq 'ticketOrdered'}">
                <p><fmt:message key="page.message.successful_order"/></p>
                <a href="${pageContext.request.contextPath}/pay-for-ticket?ticketId=${requestScope.ticketId}">
                    <fmt:message key="page.message.pay_for_ticket"/>
                </a>
                <a href="${pageContext.request.contextPath}/expositions">
                    <fmt:message key="page.message.back_to_expo_grid"/>
                </a>
            </c:when>
            <c:when test="${requestScope.event eq 'ticketPaid'}">
                <p><fmt:message key="page.message.successful_payment"/></p>
                <a href="${pageContext.request.contextPath}/tickets-list">
                    <fmt:message key="page.message.back_to_tickets"/>
                </a>
            </c:when>
        </c:choose>
    </div>
</div>
</body>
</html>
