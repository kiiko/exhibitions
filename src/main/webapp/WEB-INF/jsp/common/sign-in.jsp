<%--
  Created by IntelliJ IDEA.
  User: Kiiko
  Date: 25.01.2021
  Time: 12:42
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jspf/directives/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directives/taglib.jspf" %>
<html>
<head>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>
<body>

<div id="centerDiv">
    <%@ include file="/WEB-INF/jspf/header.jspf" %>
    <div class="section">
        <h2 style="text-align: center;"><fmt:message key="page.sign_in"/></h2>
        <div class="sign-in-container">
            <form method="post" action="sign-in">
                <label for="login"><fmt:message key="page.sign_in.login"/></label>
                <input type="text" name="login" id="login" required/>
                <label for="password"><fmt:message key="page.sign_in.password"/></label>
                <input type="password" name="password" id="password" required/>

                <input type="submit" value="<fmt:message key="page.submit"/>" class="button-submit">
            </form>
        </div>


        <c:if test="${requestScope.errorMessage != null}">
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    ${requestScope.errorMessage}
            </div>
        </c:if>

    </div>
    <div class="section">
        <p style="text-align: center;">
            <fmt:message key="page.sign_in.account_question"/>
            <a href="<c:url value="/registration"/>"> <fmt:message key="page.sign_up"/></a>
        </p>
    </div>
</div>
</body>
</html>
