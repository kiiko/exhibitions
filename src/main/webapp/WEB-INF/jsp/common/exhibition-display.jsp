<%--
  Created by IntelliJ IDEA.
  User: Kiiko
  Date: 31.01.2021
  Time: 14:21
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jspf/directives/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directives/taglib.jspf" %>
<html>
<head>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
    <script type="text/javascript">
        <fmt:message key="page.expo_grid.deleting_expo.question" var="question"/>
        let question = '<c:out value="${question}"/>';

        function confirmDeleting() {
            return confirm(question);
        }
    </script>
</head>
<body>
<div id="centerDiv">
    <%@ include file="/WEB-INF/jspf/header.jspf" %>

    <h2 style="text-align: center"><fmt:message key="page.expo_grid.title"/></h2>
    <div class="section">
        <%@ include file="/WEB-INF/jspf/search-choices.jspf" %>
    </div>
    <div class="section">
        <%@ include file="/WEB-INF/jspf/exposition-grid.jspf" %>
    </div>
    <c:if test="${requestScope.pagesCount > 1}">
        <div class="section" style="text-align: center;">
            <ul class="pagination justify-content-center">
                <c:if test="${requestScope.pagesCount != 1}">
                    <c:if test="${requestScope.currentPage != 1}">
                        <li>
                            <a href="<c:url value="/expositions?currentPage=${requestScope.currentPage - 1}${requestScope.currentGetRequestParams}"/>">
                                <fmt:message key="page.expo_grid.pages.previous"/>
                            </a>
                        </li>
                    </c:if>

                    <c:forEach begin="1" end="${requestScope.pagesCount}" var="pageNumber">
                        <c:choose>
                            <c:when test="${pageNumber == requestScope.currentPage}">
                                <li class="disabled"> <a href="#">${pageNumber}</a></li>
                            </c:when>
                            <c:otherwise>
                                <li>
                                    <a href=" <c:url value="/expositions?currentPage=${pageNumber}${requestScope.currentGetRequestParams}"/> ">${pageNumber}</a>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>

                    <c:if test="${requestScope.currentPage != requestScope.pagesCount}">
                        <li>
                            <a href="<c:url value="/expositions?currentPage=${requestScope.currentPage + 1}${requestScope.currentGetRequestParams}"/>">
                                <fmt:message key="page.expo_grid.pages.next"/>
                            </a>
                        </li>
                    </c:if>
                </c:if>
            </ul>
        </div>
    </c:if>
</div>
<script src="${pageContext.request.contextPath}/view/js/search-expo.js"></script>
</body>
</html>
