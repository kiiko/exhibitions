<%--
  Created by IntelliJ IDEA.
  User: Kiiko
  Date: 25.01.2021
  Time: 12:41
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jspf/directives/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directives/taglib.jspf" %>
<html>
<head>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>
<body>
<div id="centerDiv">
    <%@ include file="/WEB-INF/jspf/header.jspf" %>
    <div class="section">
        <h1 style="text-align: center;"><fmt:message key="page.sign_up.logo"/></h1>
        <div class="sign-up-container">
            <form method="post" action="registration" id="registration-form">
                <label for="login"><fmt:message key="page.sign_up.login"/></label>
                <input type="text" name="login" id="login" required/>

                <label for="password"><fmt:message key="page.sign_up.password"/></label>
                <input type="password" name="password" id="password" required/>

                <label for="username"><fmt:message key="page.sign_up.name"/></label>
                <input type="text" name="username" id="username" required/>

                <label for="surname"><fmt:message key="page.sign_up.surname"/></label>
                <input type="text" name="surname" id="surname" required/>

                <label for="email"><fmt:message key="page.sign_up.email"/></label>
                <input type="text" name="email" id="email" required/>

                <c:if test="${not empty sessionScope.user && sessionScope.user.role eq 'ADMIN'}">
                    <label for="userRole"><fmt:message key="page.sign_up.user_role"/></label>
                    <select name="userRole" id="userRole">
                        <option value="ADMIN"><fmt:message key="page.sign_up.admin"/></option>
                        <option value="USER" selected><fmt:message key="page.sign_up.user"/></option>
                    </select>
                </c:if>

                <input class="button-submit" type="submit" value="<fmt:message key="page.submit"/>">
            </form>
        </div>
    </div>
    <c:if test="${requestScope.errorMessage != null}">
        <div class="section">
            <div class="alert alert-danger alert-dismissible align-self-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    ${requestScope.errorMessage}
            </div>
        </div>
    </c:if>
    <c:if test="${empty sessionScope.user}">
        <div class="section">
            <p style="text-align: center;"><fmt:message key="page.sign_up.account_question"/>
                <a href="<c:url value="/sign-in"/>"> <fmt:message key="page.sign_in"/> </a>
            </p>
        </div>
    </c:if>
</div>

</body>
</html>
