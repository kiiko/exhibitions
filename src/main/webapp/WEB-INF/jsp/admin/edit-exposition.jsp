<%@ include file="/WEB-INF/jspf/directives/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directives/taglib.jspf" %>
<html>
<head>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>
<body>
<div id="centerDiv">
    <%@ include file="/WEB-INF/jspf/header.jspf" %>
    <div class="section">
        <c:if test="${requestScope.errorMessage != null}">
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    ${requestScope.errorMessage}
            </div>
        </c:if>
    </div>
    <c:if test="${empty requestScope.editingStep}">
        <div class="section">
            <form method="post" action="edit-exposition" class="times-input">
                <input type="hidden" id="periodsNumber" name="periodsNumber"
                       value="${sessionScope.expositionToBeUpdated.schedule.size()}">
                <input type="hidden" name="editingStep" value="1">
                <div class="row">
                    <div class="col-md-3">
                        <label><fmt:message key="page.expo_creating.from_date"/></label>
                    </div>
                    <div class="col-md-3">
                        <label><fmt:message key="page.expo_creating.to_date"/></label>
                    </div>
                    <div class="col-md-3">
                        <label><fmt:message key="page.expo_creating.from_time"/></label>
                    </div>
                    <div class="col-md-3">
                        <label><fmt:message key="page.expo_creating.to_time"/></label>
                    </div>
                </div>
                <div id="periods-container">
                    <c:forEach var="index" begin="0" end="${sessionScope.expositionToBeUpdated.schedule.size()-1}">
                        <c:set var="dateTimePeriod" value="${sessionScope.expositionToBeUpdated.schedule.get(index)}"
                               scope="page"/>
                        <div class="period row">
                            <div class="col-md-3">
                                <input type="date" class="form-control" id="dateStart${index+1}" name="dateFrom${index+1}"
                                       value="${dateTimePeriod.getStartDay()}" required>
                            </div>
                            <div class="col-md-3">
                                <input type="date" class="form-control" id="dateEnd${index+1}" name="dateTo${index+1}"
                                       value="${dateTimePeriod.getEndDay()}" required>
                            </div>
                            <div class="col-md-3">
                                <input type="time" class="form-control" id="timeStart${index+1}" name="timeFrom${index+1}"
                                       value="${dateTimePeriod.getStartTime()}" required>
                            </div>
                            <div class="col-md-3">
                                <input type="time" class="form-control" id="timeEnd${index+1}" name="timeTo${index+1}"
                                       value="${dateTimePeriod.getEndTime()}" required>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary"><fmt:message
                                key="page.expo_creating.next"/></button>
                    </div>
                </div>
            </form>
        </div>
    </c:if>
    <c:if test="${not empty requestScope.editingStep}">
        <div class="section">
            <div class="section">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th colspan="4" style="text-align: center; font-size: 16px">
                                    <fmt:message key="page.expo_creating.selected_dates"/>
                                </th>
                            </tr>
                            <tr>
                                <th><fmt:message key="page.expo_grid.schedule.date_from"/></th>
                                <th><fmt:message key="page.expo_grid.schedule.date_to"/></th>
                                <th><fmt:message key="page.expo_grid.schedule.time_from"/></th>
                                <th><fmt:message key="page.expo_grid.schedule.time_to"/></th>
                            </tr>
                            </thead>
                            <c:forEach var="schedule" items="${sessionScope.updatedSchedule}">
                                <tr>
                                    <td>${schedule.startDay}</td>
                                    <td>${schedule.endDay}</td>
                                    <td>${schedule.startTime}</td>
                                    <td>${schedule.endTime}</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
            <form name="editing-exposition" action="edit-exposition" method="post">
                <input type="hidden" name="editingStep" value="2">
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="themeEN"><fmt:message key="page.expo_creating.theme_in_en"/></label>
                        <input type="text" class="form-control" name="themeEN" id="themeEN"
                               value="${sessionScope.expositionToBeUpdated.expoThemeEn}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="themeUA"><fmt:message key="page.expo_creating.theme_in_ua"/></label>
                        <input type="text" class="form-control" name="themeUA" id="themeUA"
                               value="${sessionScope.expositionToBeUpdated.expoThemeUa}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="descriptionEN"><fmt:message key="page.expo_creating.description_en"/></label>
                        <input type="text" class="form-control" name="descriptionEN" id="descriptionEN"
                               value="${sessionScope.expositionToBeUpdated.descriptionEn}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="descriptionUA"><fmt:message key="page.expo_creating.description_ua"/></label>
                        <input type="text" class="form-control" name="descriptionUA" id="descriptionUA"
                               value="${sessionScope.expositionToBeUpdated.descriptionUa}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="price"><fmt:message key="page.expo_creating.price"/></label>
                        <input type="number" class="form-control" step="0.01" name="price" id="price"
                               value="${sessionScope.expositionToBeUpdated.price}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <p><fmt:message key="page.expo_editing.previous_rooms"/> ${sessionScope.expositionToBeUpdated.printRooms()}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="roomsSelection"><fmt:message key="page.expo_creating.select_rooms"/></label>
                        <select name="rooms" id="roomsSelection" class="form-control" multiple="multiple">
                            <c:forEach var="room" items="${requestScope.availableRooms}">
                                <option value="${room.roomId}">${room.floor} <fmt:message
                                        key="page.expo_creating.floor"/>
                                    - ${room.roomNum} <fmt:message key="page.expo_creating.room"/></option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><fmt:message key="page.expo_editing.save_exposition"/></button>
            </form>

        </div>
    </c:if>
</div>
</body>
</html>
