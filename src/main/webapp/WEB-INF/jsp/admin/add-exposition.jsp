<%@ include file="/WEB-INF/jspf/directives/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directives/taglib.jspf" %>
<html>
<head>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>
<body>

<div id="centerDiv">
    <%@ include file="/WEB-INF/jspf/header.jspf" %>
    <div class="section">
        <c:if test="${requestScope.errorMessage != null}">
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    ${requestScope.errorMessage}
            </div>
        </c:if>
    </div>
    <c:if test="${empty requestScope.step}">
        <h2 style="text-align: center"><fmt:message key="page.expo_creating.first_step_title"/></h2>
        <div class="section">
            <form method="post" action="add-exposition" class="times-input">
                <input type="hidden" id="periodsNumber" name="periodsNumber" value="1">
                <input type="hidden" name="step" value="1">
                <div class="row">
                    <div class="col-md-3">
                        <label for="dateStart1"><fmt:message key="page.expo_creating.from_date"/></label>
                    </div>
                    <div class="col-md-3">
                        <label for="dateEnd1"><fmt:message key="page.expo_creating.to_date"/></label>
                    </div>
                    <div class="col-md-3">
                        <label for="timeStart1"><fmt:message key="page.expo_creating.from_time"/></label>
                    </div>
                    <div class="col-md-3">
                        <label for="timeEnd1"><fmt:message key="page.expo_creating.to_time"/></label>
                    </div>
                </div>

                <div id="periods-container">
                    <div class="period row">
                        <div class="col-md-3">
                            <input type="date" class="form-control"  id="dateStart1" name="dateFrom1" required>
                        </div>
                        <div class="col-md-3">
                            <input type="date" class="form-control"  id="dateEnd1" name="dateTo1" required>
                        </div>
                        <div class="col-md-3">
                            <input type="time" class="form-control"  id="timeStart1" name="timeFrom1" required>
                        </div>
                        <div class="col-md-3">
                            <input type="time" class="form-control"  id="timeEnd1" name="timeTo1" required>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-3">
                        <button type="button" class="btn btn-secondary" onclick="addNewPeriod()">
                            <fmt:message key="page.expo_creating.add_one_more_period"/>
                        </button>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary"><fmt:message
                                key="page.expo_creating.next"/></button>
                    </div>
                </div>


            </form>
        </div>
    </c:if>
    <c:if test="${not empty requestScope.step}">
        <div class="section">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th colspan="4" style="text-align: center; font-size: 16px">
                                <fmt:message key="page.expo_creating.selected_dates"/>
                            </th>
                        </tr>
                        <tr>
                            <th><fmt:message key="page.expo_grid.schedule.date_from"/></th>
                            <th><fmt:message key="page.expo_grid.schedule.date_to"/></th>
                            <th><fmt:message key="page.expo_grid.schedule.time_from"/></th>
                            <th><fmt:message key="page.expo_grid.schedule.time_to"/></th>
                        </tr>
                        </thead>
                        <c:forEach var="schedule" items="${sessionScope.selectedSchedule}">
                            <tr>
                                <td>${schedule.startDay}</td>
                                <td>${schedule.endDay}</td>
                                <td>${schedule.startTime}</td>
                                <td>${schedule.endTime}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>
        <div class="section">
            <form method="post" action="add-exposition">
                <input type="hidden" name="step" value="2">
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="themeEN"><fmt:message key="page.expo_creating.theme_in_en"/></label>
                        <input type="text" class="form-control" name="themeEN" id="themeEN" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="themeUA"><fmt:message key="page.expo_creating.theme_in_ua"/></label>
                        <input type="text" class="form-control" name="themeUA" id="themeUA" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="descriptionEN"><fmt:message key="page.expo_creating.description_en"/></label>
                        <input type="text" class="form-control" name="descriptionEN" id="descriptionEN" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="descriptionUA"><fmt:message key="page.expo_creating.description_ua"/></label>
                        <input type="text" class="form-control" name="descriptionUA" id="descriptionUA" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="price"><fmt:message key="page.expo_creating.price"/></label>
                        <input type="number" class="form-control" step="0.01" name="price" id="price" required>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="roomsSelection"><fmt:message key="page.expo_creating.select_rooms"/></label>
                        <select name="rooms" id="roomsSelection" class="form-control" multiple="multiple">
                            <c:forEach var="room" items="${requestScope.availableRooms}">
                                <option value="${room.roomId}">${room.floor} <fmt:message
                                        key="page.expo_creating.floor"/>
                                    - ${room.roomNum} <fmt:message key="page.expo_creating.room"/></option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary"><fmt:message key="page.expo_creating.create_exposition"/></button>
            </form>
        </div>
    </c:if>

</div>
<script src="<c:url value="/view/js/expoCreating.js"/>"></script>
</body>
</html>
