<%--
  Created by IntelliJ IDEA.
  User: Kiiko
  Date: 13.02.2021
  Time: 17:41
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jspf/directives/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directives/taglib.jspf" %>
<html>
<head>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>
<body>

<div id="centerDiv">
    <%@ include file="/WEB-INF/jspf/header.jspf" %>
    <c:if test="${requestScope.errorMessage != null}">
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                ${requestScope.errorMessage}
        </div>
    </c:if>
    <div class="section">
        <div class="row">
            <div class="col-md-3"><fmt:message key="page.ticket_ordering.theme"/>:</div>
            <div class="col-md-5">${requestScope.exposition.expoTheme}</div>
        </div>
        <div class="row">
            <div class="col-md-3"><fmt:message key="page.ticket_ordering.description"/>:</div>
            <div class="col-md-5">${requestScope.exposition.description}</div>
        </div>
        <div class="row">
            <div class="col-md-3"><fmt:message key="page.ticket_ordering.price"/>:</div>
            <div class="col-md-5">${requestScope.exposition.price}</div>
            <input type="hidden" id="ticket-price" value="${requestScope.exposition.price}">
        </div>
        <div class="row">
            <div class="col-md-3"><fmt:message key="page.ticket_ordering.room"/>:</div>
            <div class="col-md-5">${requestScope.exposition.printRooms()}</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th colspan="4" style="text-align: center; font-size: 16px">
                            <fmt:message key="page.ticket_ordering.expo_schedule_title"/>
                        </th>
                    </tr>
                    <tr>
                        <th><fmt:message key="page.ticket_ordering.date_from"/></th>
                        <th><fmt:message key="page.ticket_ordering.date_to"/></th>
                        <th><fmt:message key="page.ticket_ordering.time_from"/></th>
                        <th><fmt:message key="page.ticket_ordering.time_to"/></th>
                    </tr>
                    </thead>
                    <c:forEach var="scheduleEntity" items="${requestScope.exposition.schedule}">
                        <tr>
                            <td>${scheduleEntity.startDay}</td>
                            <td>${scheduleEntity.endDay}</td>
                            <td>${scheduleEntity.startTime}</td>
                            <td>${scheduleEntity.endTime}</td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>

    <div class="section">
        <form id="ticket-ordering" method="post" action="order-ticket">
            <input type="hidden" name="expoId" value="${requestScope.exposition.expoId}"/>
            <div class="row">
                <div class="col-md-4">
                    <label for="visitorsAmount"> <fmt:message key="page.ticket_ordering.enter_number_visitors"/></label>
                </div>
                <div class="col-md-4">
                    <label for="visitingDate"><fmt:message key="page.ticket_ordering.select_date"/> </label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <input type="number" class="form-control" name="visitorsAmount" value="1" id="visitorsAmount">
                </div>
                <div class="col-md-4">
                    <input type="date" class="form-control" name="visitingDate" id="visitingDate"
                           min="${requestScope.exposition.getStartDate()}"
                           max="${requestScope.exposition.getLastDate()}"
                           value="${requestScope.exposition.getStartDate()}"
                    >
                </div>
                <div class="col-md-3">
                    <button type="button" class="btn btn-primary" onclick="addTicketInfo()" data-toggle="modal"
                            data-target="#myModal">
                        <fmt:message key="page.ticket_ordering.order_ticket"/>
                    </button>
                </div>
            </div>

            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title"><fmt:message key="page.ticket_ordering.verify_order_info"/></h4>
                        </div>

                        <div class="modal-body">
                            <h4>"${requestScope.exposition.expoTheme}"</h4>
                            <p><fmt:message key="page.ticket_ordering.selected_date"/> - <span
                                    id="selected-date-modal"></span></p>
                            <p><fmt:message key="page.ticket_ordering.visitors_count"/> - <span
                                    id="visitors-amount-modal"></span></p>
                            <p><fmt:message key="page.ticket_ordering.total_ticket_price"/> - <span
                                    id="price-modal"></span></p>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">
                                <fmt:message key="page.ticket_ordering.order_ticket"/>
                            </button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                <fmt:message key="page.ticket_ordering.return"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="${pageContext.request.contextPath}/view/js/ticketOrdering.js"></script>
</body>
</html>
