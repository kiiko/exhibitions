<%@ include file="/WEB-INF/jspf/directives/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directives/taglib.jspf" %>
<html>
<head>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>
<body>
<div id="centerDiv">
    <%@ include file="/WEB-INF/jspf/header.jspf" %>
    <div class="section">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><fmt:message
                            key="page.ticket_paying.ticket_info.ticket"/> ${requestScope.ticket.ticketId}</div>
                    <div class="panel-body">
                        <h3>${requestScope.ticket.expoTheme}</h3>
                        <p><fmt:message
                                key="page.ticket_paying.ticket_info.visitors"/>: ${requestScope.ticket.visitorsAmount}</p>
                        <p><fmt:message
                                key="page.ticket_paying.ticket_info.date"/>: ${requestScope.ticket.visitingDate}</p>
                        <p><fmt:message key="page.ticket_paying.ticket_info.price"/>: ${requestScope.ticket.price}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="payment">
            <form action="pay-for-ticket" method="post">
                <input type="hidden" name="ticketId" value="${requestScope.ticket.ticketId}">
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="cnum"><fmt:message key="page.ticket_paying.credit_card_number"/></label>
                        <input type="text" id="cnum" name="cardNumber" placeholder="1111-2222-3333-4444" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="expirationDate"><fmt:message key="page.ticket_paying.exp_month_year"/></label>
                        <input type="text" id="expirationDate" name="expirationDate" placeholder="MM/YY" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-10">
                        <label for="cvv">CVV</label>
                        <input type="number" id="cvv" name="cvv" placeholder="XXX" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><fmt:message key="page.ticket_paying.pay"/></button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
