<%@ include file="/WEB-INF/jspf/directives/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directives/taglib.jspf" %>
<html>
<head>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
    <script type="text/javascript">
        <fmt:message key="page.ticket_grid.question_before_delete_ticket" var="question"/>
        let question = '<c:out value="${question}"/>';

        function confirmDeleting() {
            return confirm(question);
        }
    </script>
</head>
<body>
<div id="centerDiv">
    <%@ include file="/WEB-INF/jspf/header.jspf" %>
    <div class="section" style="text-align: center;">
        <c:choose>
            <c:when test="${empty requestScope.userTickets}">
                <h3><fmt:message key="page.ticket_grid.no_ticket_message"/> </h3>
            </c:when>
            <c:otherwise>
                <div class="row justify-content-md-center">
                    <c:forEach var="ticket" items="${requestScope.userTickets}">
                        <c:choose>
                            <c:when test="${ticket.ticketStatus == 'UNPAID'}">
                                <div class="col-md-6">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading"><fmt:message key="page.ticket_paying.ticket_info.ticket"/> ${ticket.ticketId}</div>
                                        <div class="panel-body">
                                            <h3>${ticket.expoTheme}</h3>
                                            <p><fmt:message key="page.ticket_paying.ticket_info.visitors"/>: ${ticket.visitorsAmount}</p>
                                            <p><fmt:message key="page.ticket_paying.ticket_info.date"/>: ${ticket.visitingDate}</p>
                                            <p><fmt:message key="page.ticket_paying.ticket_info.price"/>: ${ticket.price}</p>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <form method="get" action="pay-for-ticket">
                                                        <input type="hidden" name="ticketId"
                                                               value="${ticket.ticketId}"/>
                                                        <button type="submit" class="btn btn-primary">
                                                            <fmt:message key="page.ticket_grid.pay_for_ticket"/>
                                                        </button>
                                                    </form>
                                                </div>
                                                <div class="col-md-6">
                                                    <form method="post" action="delete-ticket">
                                                        <input type="hidden" name="ticketId"
                                                               value="${ticket.ticketId}"/>
                                                        <button type="submit" class="btn btn-danger" onclick="return confirmDeleting();">
                                                            <fmt:message key="page.ticket_grid.delete_ticket"/>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-md-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><fmt:message key="page.ticket_paying.ticket_info.ticket"/> ${ticket.ticketId}</div>
                                        <div class="panel-body">
                                            <h3>${ticket.expoTheme}</h3>
                                            <p><fmt:message key="page.ticket_paying.ticket_info.visitors"/>: ${ticket.visitorsAmount}</p>
                                            <p><fmt:message key="page.ticket_paying.ticket_info.date"/>: ${ticket.visitingDate}</p>
                                            <p><fmt:message key="page.ticket_paying.ticket_info.price"/>: ${ticket.price}</p>
                                        </div>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
</html>
