function addNewPeriod() {
    let nextPeriodNumber = 1 + parseInt(document.getElementById("periodsNumber").value);

    let newPeriod = document.createElement("div");

    let inputFields = "<hr/><div class='col-md-3'><input type='date' class='form-control' id='dateStart" + nextPeriodNumber + "' name='dateFrom" + nextPeriodNumber + "' required></div> " +
        "<div class='col-md-3'><input type='date' class='form-control' id='dateEnd" + nextPeriodNumber + "' name='dateTo" + nextPeriodNumber + "' required></div>" +
        "<div class='col-md-3'><input type='time' class='form-control' id='timeStart" + nextPeriodNumber + "' name='timeFrom" + nextPeriodNumber + "' required></div>" +
        "<div class='col-md-3'><input type='time' class='form-control' id='timeEnd" + nextPeriodNumber + "' name='timeTo" + nextPeriodNumber + "' required></div>"

    newPeriod.classList.add("period", "row");
    newPeriod.innerHTML = inputFields;
    document.getElementById("periods-container").appendChild(newPeriod);
    document.getElementById("periodsNumber").value = nextPeriodNumber;
}