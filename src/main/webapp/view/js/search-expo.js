function openDetails(rowId) {
    let descriptionDiv = document.getElementById("description-" + rowId);
    if (descriptionDiv.style.display === 'none') {
        descriptionDiv.style.display = 'block';
    } else {
        descriptionDiv.style.display = 'none';
    }
}

function displayFields() {
    let searchInputDivs = ['byDate', 'byPrice', 'byTheme'];
    let searchMethods = document.getElementsByName('searchBy');
    let selectedMethod = " ";
    for (let method of searchMethods) {
        if (method.checked) {
            selectedMethod = method.value;
            break;
        }
    }
    for (let methodDiv of searchInputDivs) {
        if (methodDiv.localeCompare(selectedMethod) === 0) {
            document.getElementById(methodDiv).style.display = 'inline-block';
        } else {
            document.getElementById(methodDiv).style.display = 'none';
        }
    }
}

function clearSelection() {
    let searchInputDivs = ['byDate', 'byPrice', 'byTheme'];
    let searchRadioButtons = ['date', 'price', 'theme'];
    for (let radButton of searchRadioButtons) {
        document.getElementById('search-by-' + radButton).checked = false;
    }
    for (let methodDiv of searchInputDivs) {
        document.getElementById(methodDiv).style.display = 'none';
    }
    document.getElementById("dateStart").value = "";
    document.getElementById("dateEnd").value = "";
    document.getElementById("theme").value = "";
    document.getElementById("priceFrom").value = "";
    document.getElementById("priceTo").value = "";
}

function searchExpositions() {
    // TODO AJAX request
}

function onClickFind() {
    // TODO AJAX request
}