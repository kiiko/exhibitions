function addTicketInfo() {
    let visitorsCount = parseInt(document.getElementById('visitorsAmount').value, 10);
    let visitingDate = document.getElementById('visitingDate').value;
    let expoPrice = parseInt(document.getElementById('ticket-price').value, 10);

    document.getElementById("price-modal").innerHTML = "";
    document.getElementById("visitors-amount-modal").innerHTML = "";
    document.getElementById("selected-date-modal").innerHTML = "";

    let spanTicketPrice = document.getElementById("price-modal");
    let spanVisitorsAmount = document.getElementById("visitors-amount-modal");
    let spanSelectedDate = document.getElementById("selected-date-modal");

    let dynamicTicketPrice = document.createTextNode((expoPrice * visitorsCount).toString());
    let dynamicVisitorsAmount = document.createTextNode(visitorsCount.toString());
    let dynamicSelectedDate = document.createTextNode(visitingDate);

    spanSelectedDate.appendChild(dynamicSelectedDate);
    spanVisitorsAmount.appendChild(dynamicVisitorsAmount);
    spanTicketPrice.appendChild(dynamicTicketPrice);
}